import Dependencies._
import Versions.coreVer

/** Base project */
lazy val `prods-engine-core` = (project in file(".")).
  settings(commonSettings: _*)
  .settings(version := coreVer)
  .settings(
    libraryDependencies ++= testLib
  )

/** Product related stuff */
lazy val `prods-engine-core-product` = (project in file("./product")).
  settings(commonSettings: _*)
  .settings(version := coreVer)
  .settings(
    libraryDependencies ++= testLib
  )
  .dependsOn(`prods-engine-core`)

/** Actor related stuff */
lazy val `prods-engine-core-actor` = (project in file("./actor")).
  settings(commonSettings: _*)
  .settings(version := coreVer)
  .settings(
    libraryDependencies ++= testLib ++ akka ++ akkaPersistence
  )
  .dependsOn(`prods-engine-core`, `prods-engine-core-product`)