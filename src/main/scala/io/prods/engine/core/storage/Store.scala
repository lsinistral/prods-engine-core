package io.prods.engine.core.storage

import io.prods.engine.core._


trait Store[ITM, STG, ITI <: Iterable[ITM], IDX] {
  def addItems(items: ITI)(storage: STG): STG

  def addItem(item: ITM)(storage: STG): STG

  def getItems(ids: Set[IDX])(storage: STG): ITI

  def getItem(id: IDX)(storage: STG): Option[ITM]
}

object Store {

  implicit class StorageOps[ITM, STG, ITI <: Iterable[ITM], IDX](storage: STG)(implicit evidence: Store[ITM, STG, ITI, IDX]) {
    def add(items: ITI): STG = evidence.addItems(items)(storage)

    def add(item: ITM): STG = evidence.addItem(item)(storage)

    def get(ids: Set[IDX]): Option[ITI] = toOption(evidence.getItems(ids)(storage))

    def get(id: IDX): Option[ITM] = evidence.getItem(id)(storage)
  }


  class SeqCanStore[ITM] extends Store[ITM, Seq[ITM], Iterable[ITM], ITM] {
    def addItems(items: Iterable[ITM])(storage: Seq[ITM]): Seq[ITM] =
      storage ++ items

    def getItems(ids: Set[ITM])(storage: Seq[ITM]): Iterable[ITM] =
      storage.filter(ids.contains)

    def addItem(item: ITM)(storage: Seq[ITM]): Seq[ITM] = {
      item +: storage
    }

    def getItem(id: ITM)(storage: Seq[ITM]): Option[ITM] = {
      storage.find(_ == id)
    }
  }

}


trait DeletableItemsStore[STG, IDX] {
  def deleteItems(ids: Set[IDX])(storage: STG): STG
}

object DeletableItemsStore {

  implicit class DeletableEntitiesStoreOps[STG, IDX](storage: STG)(implicit evidence: DeletableItemsStore[STG, IDX]) {
    def delete(ids: Set[IDX]): STG = evidence.deleteItems(ids)(storage)
  }

}