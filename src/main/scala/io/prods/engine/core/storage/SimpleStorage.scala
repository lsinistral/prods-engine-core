package io.prods.engine.core.storage


case class SimpleStorage[IT](items: IT)

object SimpleStorage {

  trait SimpleStorageCanStoreSeq[ITM] extends Store[ITM, SimpleStorage[Seq[ITM]], Seq[ITM], ITM] {
    def addItems(entities: Seq[ITM])(storage: SimpleStorage[Seq[ITM]]): SimpleStorage[Seq[ITM]] =
      storage.copy(storage.items ++ entities)

    def getItems(ids: Set[ITM])(storage: SimpleStorage[Seq[ITM]]): Seq[ITM] =
      storage.items.filter(ids.contains)

    def addItem(item: ITM)(storage: SimpleStorage[Seq[ITM]]): SimpleStorage[Seq[ITM]] =
      storage.copy(item +: storage.items)

    def getItem(id: ITM)(storage: SimpleStorage[Seq[ITM]]): Option[ITM] =
      storage.items.find(_ == id)
  }

  trait SimpleStorageCanStoreSeqDelete[ITM] extends DeletableItemsStore[SimpleStorage[Seq[ITM]], ITM] {
    def deleteItems(ids: Set[ITM])(storage: SimpleStorage[Seq[ITM]]): SimpleStorage[Seq[ITM]] =
      storage.copy(storage.items.filterNot(ids.contains))
  }


  trait SimpleStorageCanStoreMaps[ID, ITM] extends Store[(ID, ITM), SimpleStorage[Map[ID, ITM]], Map[ID, ITM], ID] {
    type COL = Map[ID, ITM]

    def addItems(entities: COL)(storage: SimpleStorage[COL]): SimpleStorage[COL] =
      storage.copy(storage.items ++ entities)

    def getItems(ids: Set[ID])(storage: SimpleStorage[COL]): COL =
      storage.items.filterKeys(ids.contains)

    def addItem(item: (ID, ITM))(storage: SimpleStorage[Map[ID, ITM]]): SimpleStorage[Map[ID, ITM]] =
      storage.copy(storage.items + item)

    def getItem(id: ID)(storage: SimpleStorage[Map[ID, ITM]]): Option[(ID, ITM)] =
      storage.items.get(id).map(itm => id -> itm)
  }

  trait SimpleStorageCanStoreMapDelete[ID, ITM] extends DeletableItemsStore[SimpleStorage[Map[ID, ITM]], ID] {
    def deleteItems(ids: Set[ID])(storage: SimpleStorage[Map[ID, ITM]]): SimpleStorage[Map[ID, ITM]] =
      storage.copy(storage.items.filterKeys(!ids.contains(_)))
  }

}




