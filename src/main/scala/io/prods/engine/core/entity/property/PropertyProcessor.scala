package io.prods.engine.core.entity.property

import io.prods.engine.core.entity.Entity
import io.prods.engine.core.entity.property.PropertyProcessor.Check
import io.prods.engine.core.entity.property.PropertyProcessor.checks.Existence

import scala.collection.mutable


trait AbstractPropertyCheck

/**
  * Processes new pending properties before any entity transformations. After processing property must be mark by one
  * or more states.
  *
  * Should be one per entity type.
  *
  * @tparam P accepted property type
  * @tparam E entity type
  */

trait PropertyProcessor[P <: GenProp, E <: Entity[_]] {

  /**
    * Process single property
    *
    * @param prop   property to process
    * @param check  checks to perform. Defaults to existence check.
    * @param entity entity to process against
    *
    * @return processed properties with state
    */
  def processProperty(prop: P, check: Set[Check] = Set(Existence))(entity: E): Map[PropertyState, P]

  /**
    *
    * @param props  properties to process
    * @param check  checks to perform. Defaults to existence check.
    * @param entity entity to process against
    *
    * @return processed properties with state
    */
  def processProperties(props: Seq[P], check: Set[Check] = Set(Existence))(entity: E): Map[PropertyState, Seq[P]]
}


object PropertyProcessor {

  /**
    * Base for checks to be performed on properties
    */
  trait Check

  /**
    * Simple Checks
    */
  object checks {

    case object Existence extends Check

    case object Immutability extends Check

    case object Duplicates extends Check


  }


  /**
    * Implicit class simplifies processing interface for single property
    *
    * @param prop      property to process
    * @param processor processor implementation for given property and entity type
    * @tparam P property type
    * @tparam E entity type
    */
  implicit class ProcessPropertyOps[P <: GenProp, E <: Entity[_]](prop: P)(implicit processor: PropertyProcessor[P,
    E]) {
    def process(entity: E, check: Set[Check]): Map[PropertyState, P] = processor.processProperty(prop,
      check)(entity)
  }

  /**
    * Implicit class simplifies processing interface for single property
    *
    * @param props     properties to process
    * @param processor processor implementation for given property and entity type
    * @tparam P property type
    * @tparam E entity type
    */
  implicit class ProcessPropertiesOps[P <: GenProp, E <: Entity[_]](props: Seq[P])(implicit processor:
  PropertyProcessor[P, E]) {
    def process(entity: E, check: Set[Check]): Map[PropertyState, Seq[P]] = processor
      .processProperties(props, check)(entity)
  }

  def detectDuplicateIds[I](props: Seq[KeyProp[I]]): Set[I] = {
    val ids = mutable.HashSet.empty[I]
    val dupIds = mutable.HashSet.empty[I]

    props.foreach(prop => {
      val id = prop.id
      if (ids(id)) dupIds += id else ids += id
    })
    dupIds.toSet
  }

}
