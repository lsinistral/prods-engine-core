package io.prods.engine.core.entity

import io.prods.engine.core.entity.TransactionalEntity.TransactionCommand
import io.prods.engine.core.entity.property._

import scala.util.Try


/**
  * Transaction represents sequence of actions meant to perform in "all or none" fashion.
  * All concrete transaction types should extend this trait.
  *
  * @tparam P Property type
  */
trait Transaction[P <: GenProp] {
  def actions: Seq[TransactionCommand[P]]
}

/**
  * Simple transaction which has nothing to offer except sequence of actions.
  *
  * @param actions Actions to be performed represented by TransactionCommand
  * @tparam P Property type
  */
final class SimpleTransaction[P <: GenProp](val actions: Seq[TransactionCommand[P]]) extends Transaction[P]


object SimpleTransaction {
  def apply[P <: GenProp](command1: TransactionCommand[P], command2: TransactionCommand[P],
                          commands: TransactionCommand[P]*): SimpleTransaction[P] =
    new SimpleTransaction[P](command1 +: command2 +: commands.toSeq)
}


/**
  * Contract for entities supporting transactions.
  *
  * @tparam P Property type
  * @tparam E Entity type
  */
trait TransactionalEntity[P <: GenProp, E <: Entity[_]] {
  /**
    * Processing of whole transaction.
    *
    * @param transaction Transaction to process
    * @param entity      Entity to process
    *
    * @return Entity with transaction applied or original entity if one of the actions in transaction could not be
    *         performed
    */
  def processTransaction(transaction: Transaction[_ <: P])(entity: E): E = {
    transaction.actions.foldLeft(Some(entity): Option[E])((ent, cmd) => {
      processAction(cmd)(ent)
    }).getOrElse(entity)
  }

  /**
    * Processing of single action within transaction. Which basically consists of checking if action is valid to be
    * performed on given entity and actual application
    *
    * @param command Action to process
    * @param entity  Entity to perform action on
    * @tparam U Property type
    *
    * @return Option[Entity]. Entity with applied action or None if action could not be performed
    */
  protected def processAction[U <: P](command: TransactionCommand[U])(entity: Option[E]): Option[E] = {
    Try(performAction(checkAction(command)(entity.get).right.get)(entity.get)).toOption
  }

  /**
    * Actual action application on given entity.
    * Unsafe operation. It WILL NOT check if action is valid or not.
    *
    * @param command Action to perform. Should be valid otherwise application of that action may corrupt entity data
    * @param entity Entity to perform action on
    * @tparam U Property type
    *
    * @return Entity with action performed on it
    */
  protected def performAction[U <: P](command: TransactionCommand[U])(entity: E): E

  /**
    * Checks if action is valid for the given property
    *
    * @param command Action to validate
    * @param entity Entity to validate action against
    * @tparam U Property type
    *
    * @return Either action itself or set of reasons why action is not valid.
    */
  protected def checkAction[U <: P](command: TransactionCommand[U])(entity: E): Either[Set[PropertyState],
    TransactionCommand[U]]
}

object TransactionalEntity {

  trait TransactionCommand[P <: GenProp]

  final case class AddProperties[P <: GenProp](properties: Seq[P]) extends TransactionCommand[P]

  final case class AddProperty[P <: GenProp](property: P) extends TransactionCommand[P]

  final case class ChangeProperties[P <: GenProp](properties: Seq[P]) extends TransactionCommand[P]

  final case class ChangeProperty[P <: GenProp](property: P) extends TransactionCommand[P]

}


object EnableTransactions {

  implicit class TransactionOps[P <: GenProp, E <: Entity[_]](entity: E)
                                                             (implicit evidence: TransactionalEntity[P, E]) {
    def transaction(transaction: Transaction[_ <: P]): E =
      implicitly[TransactionalEntity[P, E]]
        .processTransaction(transaction)(entity)
  }

}
