package io.prods.engine.core.entity.property.properties

import io.prods.engine.core.entity.property.ValueType
import io.prods.engine.core.entity.property.Property

import scala.language.implicitConversions


case class NamedProperty[K, V](id: K, value: V, name: ValueType, weight: Int) extends Property[K, V]

/** Experimental stuff below */
case class Named[K, V](id: K, value: V, name: ValueType, weight: Int)

object Named {


  implicit class NamedCanBeProperty[K, V](named: Named[K, V]) extends Property[K, (V, ValueType, Int)] {
    val id = named.id

    val value = (named.value, named.name, named.weight)

    def asProperty = new NamedCanBeProperty(named)

    override def toString = s"Named property ${named.id}: name= ${named.name} value= ${named.value}"
  }

  implicit class PropertyCanBeNamed[K, N, V <: (N, ValueType, Int)](prop: Property[K, V]) {
    def asNamed = new Named(prop.id, prop.value._1, prop.value._2, prop.value._3)
  }

}

object NamedWithNoWeight {

  implicit class NamedCanBePropertyNoWeight[K, V](named: Named[K, V]) extends Property[K, (V, ValueType)] {
    val id = named.id

    val value = (named.value, named.name)

    def asProperty = new NamedCanBePropertyNoWeight(named)

    override def toString = s"Named property ${named.id}: name: ${named.name} value: ${named.value}"
  }

  implicit class PropertyCanBeNamed[K, N, V <: (N, ValueType)](prop: Property[K, V]) {
    def asNamed = new Named(prop.id, prop.value._1, prop.value._2, 1)
  }

}