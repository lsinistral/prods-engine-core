package io.prods.engine.core.entity

import io.prods.engine.core.entity.property.GenProp


trait EntityEvent

trait EntityPropertyEvent[P <: GenProp] extends EntityEvent

final case class PropertyChanged[P <: GenProp](property: P) extends EntityPropertyEvent[P]

final case class PropertyAdded[P <: GenProp](property: P) extends EntityPropertyEvent[P]