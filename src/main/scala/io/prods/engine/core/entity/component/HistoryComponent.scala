package io.prods.engine.core.entity.component

import io.prods.engine.core.entity._
import io.prods.engine.core.entity.property.{ValidatedProperty, KeyProp}


object HistoryComponent {

  trait History[K] {
    val history: EntityHistory[K]
  }

  trait ExtensibleHistoryValidation[K, E <: Entity[_] with History[K]]
    extends ExtensibleEntityValidation[KeyProp[K], E] {

    implicit object ExtensibleValidation extends EntityHistory.SimpleEntityHistoryContract[K]

    def addValidation(prop: KeyProp[K])(entity: E): ValidatedProperty[KeyProp[K]] =
      entity.history.validateAddition(prop)

    def addValidation(props: Seq[KeyProp[K]])(entity: E): Seq[ValidatedProperty[KeyProp[K]]] =
      entity.history.validateAddition(props)
  }

  trait ChangeableHistoryValidation[K, E <: Entity[_] with History[K]]
    extends ChangeableEntityValidation[KeyProp[K], E] {

    implicit object ChangeableValidation extends EntityHistory.SimpleEntityHistoryContract[K]

    def changeValidation(prop: KeyProp[K])(entity: E): ValidatedProperty[KeyProp[K]] =
      entity.history.validateChanges(prop)

    def changeValidation(props: Seq[KeyProp[K]])(entity: E): Seq[ValidatedProperty[KeyProp[K]]] =
      entity.history.validateChanges(props)
  }

}
