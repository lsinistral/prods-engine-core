package io.prods.engine.core.entity.property.properties

import io.prods.engine.core.entity.property.Property

case class NamelessProperty[K, +V](id: K, value: V, weight: Int) extends Property[K, V]
