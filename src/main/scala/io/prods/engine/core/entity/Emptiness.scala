package io.prods.engine.core.entity


trait Emptiness {
  def isEmpty: Boolean

  def nonEmpty: Boolean = !isEmpty
}
