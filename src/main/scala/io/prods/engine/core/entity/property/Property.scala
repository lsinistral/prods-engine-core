package io.prods.engine.core.entity.property

import scala.language.implicitConversions


/**
  * Marker trait for properties
  * No requirements
  */
trait AbstractProperty


/**
  * Main contract for property to implement
  *
  * @tparam K identifier type. Same within entity. Value should be unique within entity.
  * @tparam V value type. One entity may support multi type values.
  */
trait Property[K, +V] extends AbstractProperty {
  def id: K

  def value: V
}

object Property {
  /**
    * Converts single property to one item sequence of properties.
    *
    * @param property property to convert
    * @tparam K property identifier type
    * @tparam V property value type
    *
    * @return sequence of one property
    */
  implicit def propertyToSeq[K, V](property: Property[K, V]): Seq[Property[K, V]] = Seq(property)
}






