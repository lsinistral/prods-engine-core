package io.prods.engine.core.entity.component

import io.prods.engine.core.entity._
import Entity.Memory
import TransactionalEntity._
import io.prods.engine.core.entity.property.PropertyProcessor._
import io.prods.engine.core.entity.property._

import scala.collection.mutable
import scala.util._


class EntityHistory[K](val id: Int, val ids: Set[K], val memory: Memory[KeyProp[K]])
  extends Entity[Int] with Emptiness {
  def copy(id: Int = id, ids: Set[K] = ids, memory: Memory[KeyProp[K]] = memory) = new EntityHistory(id, ids, memory)

  def isEmpty = ids.isEmpty && memory.isEmpty
  
  override def toString =
    s"History #$id. ids in history: ${ids.size}, events in memory: ${memory.size}, " +
      s"last event: ${Try(memory.head).getOrElse("Unknown")}, key type: ${Try(ids.head.getClass).getOrElse("Unknown")}"
}

object EntityHistory extends Entity.EntityOps {

  def empty[K](id: Int = 0) = new EntityHistory(id, Set.empty[K], Seq.empty[EntityPropertyEvent[Property[K, _]]])


  trait ExtensibleEntityHistory[K] extends ExtensibleEntity[KeyProp[K], EntityHistory[K]] {
    def addProperties(props: Seq[KeyProp[K]])(entity: EntityHistory[K]): EntityHistory[K] = {
      val (newIds, events) = props.map(prop => (prop.id, PropertyAdded(prop))).unzip
      entity.copy(ids = entity.ids ++ newIds, memory = events ++: entity.memory)
    }

    def addProperty(prop: KeyProp[K])(entity: EntityHistory[K]): EntityHistory[K] = {
      entity.copy(ids = entity.ids + prop.id, memory = PropertyAdded(prop) +: entity.memory)
    }
  }

  trait ChangeableEntityHistory[K] extends
    ChangeableEntity[KeyProp[K], EntityHistory[K]] {
    def changeProperties(props: Seq[KeyProp[K]])(entity: EntityHistory[K]): EntityHistory[K] = {
      val events = props.map(prop => PropertyChanged(prop))
      entity.copy(memory = events ++: entity.memory)
    }

    def changeProperty(prop: KeyProp[K])(entity: EntityHistory[K]): EntityHistory[K] = {
      entity.copy(memory = PropertyChanged(prop) +: entity.memory)
    }
  }

  trait ExtensibleValidation[I] extends
    ExtensibleEntityValidation[KeyProp[I], EntityHistory[I]] {
    this: PropertyProcessor[KeyProp[I], EntityHistory[I]] =>


    def addValidation(prop: KeyProp[I])(entity: EntityHistory[I]): ValidatedProperty[KeyProp[I]] = {

      processProperty(prop)(entity).map({
        case (NotExist, p) => ValidProperty(p)
        case (Exist, p) => InvalidProperty(p, Some(Set(Exist)))
      }).head
    }

    def addValidation(props: Seq[KeyProp[I]])(entity: EntityHistory[I]):
    Seq[ValidatedProperty[KeyProp[I]]] = {
      val processed = processProperties(props, Set(checks.Duplicates))(entity)

      (processed(NotExist) diff processed(Duplicate)).map(p => ValidProperty(p)) ++
        (processed(Exist) diff processed(Duplicate)).map(InvalidProperty(_, Some(Set(Exist)))) ++
        (processed(Exist) intersect processed(Duplicate)).map(InvalidProperty(_, Some(Set(Exist, Duplicate)))) ++
        (processed(Duplicate) diff processed(Exist)).map(InvalidProperty(_, Some(Set(Duplicate))))
    }
  }

  trait ChangeableValidation[I] extends
    ChangeableEntityValidation[KeyProp[I], EntityHistory[I]] {
    this: PropertyProcessor[KeyProp[I], EntityHistory[I]] =>


    def changeValidation(prop: KeyProp[I])
                        (entity: EntityHistory[I]): ValidatedProperty[KeyProp[I]] = {

      processProperty(prop)(entity).map({
        case (Exist, p) => ValidProperty(p)
        case (NotExist, p) => InvalidProperty(p, Some(Set(NotExist)))
      }).head

    }

    def changeValidation(props: Seq[KeyProp[I]])
                        (entity: EntityHistory[I]): Seq[ValidatedProperty[KeyProp[I]]] = {
      val processed = processProperties(props, Set(checks.Duplicates))(entity)

      (processed(Exist) diff processed(Duplicate)).map(p => ValidProperty(p)) ++
        (processed(NotExist) diff processed(Duplicate)).map(InvalidProperty(_, Some(Set(NotExist)))) ++
        (processed(NotExist) intersect processed(Duplicate)).map(InvalidProperty(_, Some(Set(NotExist, Duplicate)))) ++
        (processed(Duplicate) diff processed(NotExist)).map(InvalidProperty(_, Some(Set(Duplicate))))

    }
  }


  trait HistoryProcessor[I] extends
    PropertyProcessor[KeyProp[I], EntityHistory[I]] {
    def processProperty(prop: KeyProp[I], check: Set[Check])
                       (entity: EntityHistory[I]): Map[PropertyState, KeyProp[I]] = {
      if (entity.ids.contains(prop.id)) Map(Exist -> prop) else Map(NotExist -> prop)
    }

    def processProperties(props: Seq[KeyProp[I]], check: Set[Check])
                         (entity: EntityHistory[I]): Map[PropertyState, Seq[KeyProp[I]]] = {

      val out = new mutable.HashMap[PropertyState, mutable.ListBuffer[KeyProp[I]]]

      if (check.contains(checks.Duplicates)) out(Duplicate) = mutable.ListBuffer.empty
      out(Exist) = mutable.ListBuffer.empty
      out(NotExist) = mutable.ListBuffer.empty

      val dupIds: Set[I] =
        if (check.contains(checks.Duplicates)) detectDuplicateIds(props)
        else Set.empty

      val checkDuplicate: (KeyProp[I]) => KeyProp[I] = prop => {
        if (dupIds.contains(prop.id)) out(Duplicate) += prop

        prop
      }

      val checkExistence: (KeyProp[I]) => KeyProp[I] = prop => {
        if (entity.ids.contains(prop.id)) out(Exist) += prop else out(NotExist) += prop
        prop
      }

      val process: (KeyProp[I]) => KeyProp[I] = if (check.contains(checks.Duplicates)) {
        checkDuplicate andThen checkExistence
      }
      else checkExistence

      props.foreach(prop => {
        process(prop)
      })

      out.mapValues(v => v.toSeq).toMap
    }
  }


  trait TransactionalEntityHistory[K]
    extends TransactionalEntity[KeyProp[K], EntityHistory[K]] {

    this: ExtensibleEntity[KeyProp[K], EntityHistory[K]]
      with ChangeableEntity[KeyProp[K], EntityHistory[K]]
      with ChangeableEntityValidation[KeyProp[K], EntityHistory[K]]
      with ExtensibleEntityValidation[KeyProp[K], EntityHistory[K]] =>

    def performAction[U <: KeyProp[K]](command: TransactionCommand[U])(entity: EntityHistory[K]):
    EntityHistory[K] = {
      command match {
        case AddProperty(p) => addProperty(p)(entity)
        case AddProperties(p) => addProperties(p)(entity)
        case ChangeProperty(p) => changeProperty(p)(entity)
        case ChangeProperties(p) => addProperties(p)(entity)
      }

    }

    protected def checkAction[U <: KeyProp[K]](command: TransactionCommand[U])(entity: EntityHistory[K]): scala
    .Either[Set[PropertyState], TransactionCommand[U]] = {
      command match {
        case c@AddProperty(p) if addValidation(p)(entity).isValid => Right(c)
        case c@ChangeProperty(p) if changeValidation(p)(entity).isValid => Right(c)

        case c@AddProperties(p) if addValidation(p)(entity).forall(_.isValid) => Right(c)
        case c@ChangeProperties(p) if changeValidation(p)(entity).forall(_.isValid) => Right(c)

        case _ => Left(Set.empty)
      }
    }
  }


  trait SimpleEntityHistoryContract[I]
    extends ExtensibleEntityHistory[I]
            with ChangeableEntityHistory[I]
            with ExtensibleValidation[I]
            with ChangeableValidation[I]
            with HistoryProcessor[I]

  trait TransactionalEntityHistoryContract[I]
    extends SimpleEntityHistoryContract[I]
            with TransactionalEntityHistory[I]

  implicit object Int extends TransactionalEntityHistoryContract[Int]

  implicit object String extends TransactionalEntityHistoryContract[String]


}