package io.prods.engine.core.entity

import io.prods.engine.core.entity.property._
import scala.collection.immutable


/**
  * This trait represents very basic and quite useless entity
  * only requirement for entity is to have unique value to be identified.
  *
  * @tparam ID entity unique identificator type
  */
trait Entity[ID] {
  def id: ID
}

/**
  * Contract for entities which wants to add properties somehow
  *
  * @tparam P property type
  * @tparam E entity type
  */
trait ExtensibleEntity[P <: GenProp, E <: Entity[_]] {
  def addProperties(props: Seq[P])(entity: E): E

  def addProperty(prop: P)(entity: E): E
}

/**
  * Validation performed to add properties to entity
  *
  * @tparam P property type
  * @tparam E entity type
  */
trait ExtensibleEntityValidation[P <: GenProp, E <: Entity[_]] {
  def addValidation(prop: P)(entity: E): ValidatedProperty[P]

  def addValidation(props: Seq[P])(entity: E): Seq[ValidatedProperty[P]]
}

/**
  * Contract for entities which wants to change properties somehow
  *
  * @tparam P property type
  * @tparam E entity type
  */
trait ChangeableEntity[P <: GenProp, E <: Entity[_]] {
  def changeProperties(props: Seq[P])(entity: E): E

  def changeProperty(prop: P)(entity: E): E
}

/**
  * Validation performed to change properties to entity
  *
  * @tparam P property type
  * @tparam E entity type
  */
trait ChangeableEntityValidation[P <: GenProp, E <: Entity[_]] {
  def changeValidation(prop: P)(entity: E): ValidatedProperty[P]

  def changeValidation(props: Seq[P])(entity: E): Seq[ValidatedProperty[P]]
}


object Entity {

  type FullState[ID, V] = Map[ID, V]

  type Memory[P <: GenProp] = Seq[EntityPropertyEvent[P]]

  /**
    * Unsafe (a.k.a no validation) actions on entity with validation.
    */
  trait UnsafeEntityOps {

    /**
      * Implicit class provides convenient interface for property addition
      *
      * @param entity   entity to add to
      * @param evidence implementation of Extensible contract for this entity
      * @tparam P property type
      * @tparam E entity type
      */
    implicit class ExtensibleEntityOps[P <: GenProp, E <: Entity[_]](entity: E)
                                                                    (implicit evidence: ExtensibleEntity[P, E]) {
      def and(prop: P): E =
        evidence.addProperty(prop)(entity)


      def and(props: Seq[P]): E =
        evidence.addProperties(props)(entity)

      def and(prop1: P, propN: P*): E = and(prop1 +: propN)
    }

    /**
      * Implicit class provides convenient interface for property changing
      *
      * @param entity   entity to add to
      * @param evidence implementation of Extensible contract for this entity
      * @tparam P property type
      * @tparam E entity type
      */
    implicit class ChangeableEntityOps[P <: GenProp, E <: Entity[_]](entity: E)
                                                                    (implicit evidence: ChangeableEntity[P, E]) {
      def change(prop: P): E =
        evidence.changeProperty(prop)(entity)

      def change(props: Seq[P]): E =
        evidence.changeProperties(props)(entity)

      def change(prop1: P, propN: P*): E = change(prop1 +: propN)

    }

  }

  /**
    * Extensible entity property validation actions implementation.
    *
    * @tparam P Property type
    * @tparam E Entity type
    */
  trait ExtensibleValidation[P <: GenProp, E <: Entity[_]] {
    val entity: E
    implicit val validation: ExtensibleEntityValidation[P, E]

    def validateAddition(prop: P): ValidatedProperty[P] =
      validation.addValidation(prop)(entity)

    def validateAddition(prop: Seq[P]): Seq[ValidatedProperty[P]] =
      validation.addValidation(prop)(entity)
  }

  /**
    * Changeable entity property validation actions implementation.
    *
    * @tparam P Property type
    * @tparam E Entity type
    */
  trait ChangeableValidation[P <: GenProp, E <: Entity[_]] {
    val entity: E
    implicit val validation: ChangeableEntityValidation[P, E]

    def validateChanges(prop: P): ValidatedProperty[P] =
      validation.changeValidation(prop)(entity)

    def validateChanges(prop: Seq[P]): Seq[ValidatedProperty[P]] =
      validation.changeValidation(prop)(entity)
  }

  object Util {
    /**
      * Unwraps collection of ValidatedProperties to collection of Properties
      *
      * @param validated collection of ValidatedProperty
      * @tparam P property type
      *
      * @return collection of Property
      */
    def unwrapValid[P <: GenProp](validated: Seq[ValidatedProperty[P]]): immutable.Seq[P] = {
      validated.foldLeft(immutable.Seq.empty[P])((seq, pr) => pr match {
        case ValidProperty(p, _) => p +: seq
        case _ => seq
      })
    }

    def unwrapValid[P <: GenProp](validated: ValidatedProperty[P]): Option[P] = {
      validated match {
        case ValidProperty(p, _) => Some(p)
        case _ => None
      }
    }


    implicit class UnwrapValidated[P <: GenProp](validated: Seq[ValidatedProperty[P]]) {
      def valid: immutable.Seq[P] = unwrapValid(validated)
    }

    implicit class UnwrapOneValidated[P <: GenProp](validated: ValidatedProperty[P]) {
      def valid: Option[P] = unwrapValid(validated)
    }

  }


  /**
    * Regular actions on entity with validation.
    */
  trait EntityOps {

    /**
      * Implicit class provides convenient interface for property addition
      *
      * @param entity     entity to add to
      * @param evidence   implementation of Extensible contract for this entity
      * @param validation implementation of Validation for this entity
      * @tparam P property type
      * @tparam E entity type
      */
    implicit class ExtensibleEntityOps[P <: GenProp, E <: Entity[_]](val entity: E)
                                                                    (implicit val evidence: ExtensibleEntity[P, E],
                                                                     val validation: ExtensibleEntityValidation[P, E])
      extends UnsafeOps.ExtensibleEntityOps[P, E](entity) with ExtensibleValidation[P, E] {


      import Util._


      override def and(prop: P): E = {
        validateAddition(prop) match {
          case ValidProperty(pr, _) => super.and(pr)

          case _ => entity
        }
      }

      override def and(props: Seq[P]): E =
        super.and(validateAddition(props).valid)

    }

    /**
      * Implicit class provides convenient interface for property changing
      *
      * @param entity     entity to add to
      * @param evidence   implementation of Extensible contract for this entity
      * @param validation implementation of Validation for this entity
      * @tparam P property type
      * @tparam E entity type
      */
    implicit class ChangeableEntityOps[P <: GenProp, E <: Entity[_]](val entity: E)
                                                                    (implicit val evidence: ChangeableEntity[P, E],
                                                                     val validation: ChangeableEntityValidation[P, E])
      extends UnsafeOps.ChangeableEntityOps[P, E](entity) with ChangeableValidation[P, E] {


      import Util._


      override def change(prop: P): E = {
        validateChanges(prop) match {
          case ValidProperty(pr, _) => super.change(prop)

          case _ => entity
        }
      }

      override def change(props: Seq[P]): E =
        super.change(validateChanges(props).valid)
    }

  }

  /**
    * Validation actions on entity
    */
  trait EntityValidationOps {

    implicit class ExtensibleValidationOps[P <: GenProp, E <: Entity[_]](val entity: E)
                                                                        (implicit val validation:
                                                                        ExtensibleEntityValidation[P, E])
      extends ExtensibleValidation[P, E]

    implicit class ChangeableValidationOps[P <: GenProp, E <: Entity[_]](val entity: E)
                                                                        (implicit val validation:
                                                                        ChangeableEntityValidation[P, E])
      extends ChangeableValidation[P, E]

  }

  object Ops extends EntityOps

  object UnsafeOps extends UnsafeEntityOps

  object ValidationOps extends EntityValidationOps

}
