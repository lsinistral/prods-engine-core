package io.prods.engine.core.entity.property


/**
  * After processing by entity&#96;s property processor properties will have some state,
  * this is root of all states
  */
trait PropertyState

case object Exist extends PropertyState

case object NotExist extends PropertyState

case object Duplicate extends PropertyState

case object CantModify extends PropertyState


