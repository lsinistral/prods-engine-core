package io.prods.engine.core.entity.property

import java.util.Locale

/** Trait represents value types of properties
  *
  */
sealed trait ValueType {
  def value: Any
}

/**
  * Represents value types suitable for naming purposes.
  */
sealed trait Nominate extends ValueType


object ValueTypes {

  final case class Text(value: String) extends Nominate

  final case class MultiLang(value: Map[Locale, Text]) extends Nominate

  final case class Number(value: Int) extends ValueType

  final case class Currency(value: BigDecimal) extends ValueType

  final case class Blob(value: Array[Byte]) extends ValueType

  final case class ValueArray(value: Array[ValueType]) extends ValueType

  case object NoValue extends ValueType {
    val value = None
  }

}