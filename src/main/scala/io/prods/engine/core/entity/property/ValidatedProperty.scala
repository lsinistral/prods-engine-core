package io.prods.engine.core.entity.property


sealed trait ValidatedProperty[P <: GenProp] extends AbstractProperty {
  def property: P

  def state: Option[Set[PropertyState]]

  def isValid: Boolean
}

final case class ValidProperty[P <: GenProp](property: P, state: Option[Set[PropertyState]] = None) extends
  ValidatedProperty[P] {
  val isValid = true
}


final case class InvalidProperty[P <: GenProp](property: P, state: Option[Set[PropertyState]] = None) extends
  ValidatedProperty[P] {
  val isValid = false
}

