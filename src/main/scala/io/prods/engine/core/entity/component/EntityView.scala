package io.prods.engine.core.entity.component

import io.prods.engine.core.entity._
import Entity._
import io.prods.engine.core.entity.property._

import scala.util.Try


class EntityView[K](val id: Int, val state: FullState[K, _]) extends Entity[Int] {

  def copy(id: Int = id, state: FullState[K, _] = state) = new EntityView(id, state)

  override def toString =
    s"View #$id. items in state: ${state.size}, key type: ${Try(state.keys.head.getClass).getOrElse("Unknown")}"
}


object EntityView {

  def empty[K](id: Int = 0) = new EntityView[K](id, Map.empty)

  trait ExtensibleEntityView[K] extends ExtensibleEntity[KeyProp[K], EntityView[K]] {
    def addProperties(props: Seq[KeyProp[K]])(entity: EntityView[K]): EntityView[K] =
      entity.copy(state = entity.state ++ props.map(prop => prop.id -> prop).toMap)

    def addProperty(prop: KeyProp[K])(entity: EntityView[K]): EntityView[K] =
      entity.copy(state = entity.state + (prop.id -> prop))
  }

  trait ChangeableEntityView[K] extends ChangeableEntity[KeyProp[K], EntityView[K]] {
    def changeProperties(props: Seq[KeyProp[K]])(entity: EntityView[K]): EntityView[K] =
      entity.copy(state = entity.state ++ props.map(prop => prop.id -> prop).toMap)

    def changeProperty(prop: KeyProp[K])(entity: EntityView[K]): EntityView[K] =
      entity.copy(state = entity.state + (prop.id -> prop))
  }


  object Ops extends Entity.UnsafeEntityOps {

    implicit object Int extends ExtensibleEntityView[Int] with ChangeableEntityView[Int]

    implicit object String extends ExtensibleEntityView[String] with ChangeableEntityView[String]

  }

}