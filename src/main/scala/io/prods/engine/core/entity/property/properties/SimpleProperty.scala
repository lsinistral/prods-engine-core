package io.prods.engine.core.entity.property.properties

import io.prods.engine.core.entity.property.Property


/**
  * Minimal property implementing Property contract
  *
  * @param id    unique identifier. Must be unique within entity
  * @param value value of the property
  * @tparam K identifier type
  * @tparam V value type
  */
case class SimpleProperty[K, +V](id: K, value: V) extends Property[K, V]