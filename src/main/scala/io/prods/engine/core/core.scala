package io.prods.engine

import java.util.UUID


package object core {
  type EntityId = UUID

  def generateID: EntityId = {
    UUID.randomUUID()
  }

  def toOption[I <: Iterable[_]](iterable: I): Option[I] = {
    Option(iterable).filterNot(_.isEmpty).orElse(None)
  }
}
