package io.prods.engine.core.test.share

import org.scalatest.{Matchers, WordSpec}

abstract class UnitSpec extends WordSpec with Matchers
