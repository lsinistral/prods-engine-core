package io.prods.engine.core.test

import io.prods.engine.core.entity.TransactionalEntity.{AddProperty, ChangeProperty}
import io.prods.engine.core.entity.{SimpleTransaction, TransactionalEntity}
import io.prods.engine.core.test.share.{MockTypeclassProperties, UnitSpec}


class TransactionSpec extends UnitSpec with MockTypeclassProperties {
  "SimpleTransaction" when {
    "created" must {
      "accept not less than 2 commands" in {
        "SimpleTransaction(AddProperty(propNew1), AddProperty(propNew2))" should compile
        "SimpleTransaction(AddProperty(propNew1), AddProperty(propNew2), AddProperty(propNew3))" should compile
        "SimpleTransaction(AddProperty(propNew1))" shouldNot compile
      }
      "accept properties only with same ID class" in {
        val add1 = AddProperty(propNew1)
        val wrongType = AddProperty(propString)

        "SimpleTransaction(add1, wrongType)" shouldNot compile
      }
      "produce sequences with right command order" in {
        val add1 = AddProperty(propNew1)
        val add2 = AddProperty(propNew2)
        val add3 = AddProperty(propNew3)
        val change1 = ChangeProperty(propNewDuplicate1)


        val transOfTwo = SimpleTransaction(add1, add2)
        val transOfThree = SimpleTransaction(add1, add2, add3)
        val transOfFour = SimpleTransaction(add1, add2, add3, change1)

        transOfTwo.actions shouldBe Seq(add1, add2)
        transOfThree.actions shouldBe Seq(add1, add2, add3)
        transOfFour.actions shouldBe Seq(add1, add2, add3, change1)

      }

    }
  }
}