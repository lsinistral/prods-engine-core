package io.prods.engine.core.test

import io.prods.engine.core.storage.SimpleStorage.{SimpleStorageCanStoreSeq, SimpleStorageCanStoreMaps}
import io.prods.engine.core.storage._
import io.prods.engine.core.test.share.UnitSpec


trait StoreBehavior extends UnitSpec {

  import Store.StorageOps

  def storage[ITM, ID, ITI <: Iterable[ITM], ST, STORE <: Store[ITM, ST, ITI, ID]](getStorage: => ST,
                                                                                   add: (ITI, ST),
                                                                                   singleItem: (ITM, ST),
                                                                                   existent: (Set[ID], ITI),
                                                                                   nonExistentId: Set[ID])
                                                                                  (implicit storeImpl: STORE) = {

    implicit class StoreOps(store: ST) extends StorageOps[ITM, ST, ITI, ID](store)(storeImpl)

    val addition = add._1
    val withAdded = add._2

    "add entities to storage" in {
      var storage = getStorage
      storage = storage add addition
      storage shouldBe withAdded
    }
    "add single entity to storage" in {
      var storage = getStorage
      storage = storage add singleItem._1
      storage shouldBe singleItem._2
    }
    "get existent entities" in {
      val storage = getStorage add addition

      (storage get existent._1) shouldBe Some(existent._2)
    }
    "get single item" in {
      val storage = getStorage add addition

      (storage get existent._1.head) shouldBe Some(existent._2.head)
    }
    "get None for non existent entities" in {
      val storage = getStorage add addition

      (storage get nonExistentId) shouldBe None
    }
  }
}

class StoreSpec extends UnitSpec with StoreBehavior {
  "StorageOps" when {
    "mixed in" must {
      "" in {

      }
    }
  }
  "Simple Storage" when {
    "entities are Map" must {
      type ITM = (Int, String)
      type ID = Int
      type ITI = Map[Int, String]
      type ST = SimpleStorage[ITI]

      object Impl extends SimpleStorageCanStoreMaps[Int, String]
      val items = (1 -> "one", 2 -> "two", 3 -> "three")
      val addition = Map(items._1, items._2, items._3)

      behave like storage[ITM, ID, ITI, ST, Store[ITM, ST, ITI, ID]](
        SimpleStorage(Map.empty[Int, String]),
        addition -> SimpleStorage(addition),
        (1 -> "one") -> SimpleStorage(Map(1 -> "one")),
        Set(1) -> Map(items._1),
        Set(4)
      )(Impl)

    }
    "entities are Seq" must {
      type ITM = Int
      type ID = Int
      type ITI = Seq[Int]
      type ST = SimpleStorage[ITI]

      object Impl extends SimpleStorageCanStoreSeq[Int]
      behave like storage[ITM, ID, ITI, ST, Store[ITM, ST, ITI, ID]](new SimpleStorage(Seq.empty[Int]),
        Seq(1, 2, 3) -> SimpleStorage(Seq(1, 2, 3)),
        1 -> SimpleStorage(Seq(1)),
        Set(1) -> Seq(1),
        Set(4)
      )(Impl)
    }
  }
}
