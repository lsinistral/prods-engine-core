package io.prods.engine.core.test

import io.prods.engine.core.entity.Entity.Memory
import io.prods.engine.core.entity.TransactionalEntity._
import io.prods.engine.core.entity._
import io.prods.engine.core.entity.component.{EntityHistory, EntityView}
import io.prods.engine.core.entity.property.KeyProp
import io.prods.engine.core.test.share.{MockTypeclassProperties, UnitSpec}


class EntitySpec extends UnitSpec with MockTypeclassProperties {


  "EntityHistory" when {
    import EntityHistory._
    val getEmptyEntity = EntityHistory.empty[Int]()
    val getFilledEntity = EntityHistory.empty[Int]().and(propInState1, propInState2, propInState3)
    val emptyMemory = Seq.empty[Memory[KeyProp[Int]]]
    val initialMemory = Seq(PropertyAdded(propInState3), PropertyAdded(propInState2), PropertyAdded
    (propInState1))
    val initialIds = Set(propInState1.id, propInState2.id, propInState3.id)

    "extensible" must {
      "accept only properly typed properties" in {
        "EntityHistory.empty[Int]().add(propString)" shouldNot typeCheck
      }
      "add new properties" in {
        val ent = getEmptyEntity

        val single = ent.and(propNew1)
        val multi = ent.and(Seq(propNew1, propNew2))
        val vararg = ent.and(propNew1, propNew2, propNew3)

        single.memory shouldBe Seq(PropertyAdded(propNew1))
        multi.memory shouldBe Seq(PropertyAdded(propNew2), PropertyAdded(propNew1))
        vararg.memory shouldBe Seq(PropertyAdded(propNew3), PropertyAdded(propNew2), PropertyAdded(propNew1))

        single.ids shouldBe Set(propNew1.id)
        multi.ids shouldBe Set(propNew1.id, propNew2.id)
        vararg.ids shouldBe Set(propNew1.id, propNew2.id, propNew3.id)

      }
      "do not add existent properties" in {
        val ent = getFilledEntity

        val initialMemory = Seq(PropertyAdded(propInState3), PropertyAdded(propInState2), PropertyAdded
        (propInState1))

        val initialIds = Set(propInState1.id, propInState2.id, propInState3.id)

        val singleExistent = ent.and(propInStateChanges1)
        val multiAllExistent = ent.and(Seq(propInStateChanges1, propInStateChanges2))
        val varargAllExistent = ent.and(propInStateChanges1, propInStateChanges2, propInStateChanges3)

        singleExistent.memory shouldBe initialMemory
        multiAllExistent.memory shouldBe initialMemory
        varargAllExistent.memory shouldBe initialMemory

        singleExistent.ids shouldBe initialIds
        multiAllExistent.ids shouldBe initialIds
        varargAllExistent.ids shouldBe initialIds


        val multiMixedExistent = ent.and(Seq(propNew1, propInStateChanges2))
        val varargMixedExistent = ent.and(propNew1, propInStateChanges2, propInStateChanges3)

        multiMixedExistent.memory shouldBe PropertyAdded(propNew1) +: initialMemory
        varargMixedExistent.memory shouldBe PropertyAdded(propNew1) +: initialMemory

        multiMixedExistent.ids shouldBe initialIds + propNew1.id
        varargMixedExistent.ids shouldBe initialIds + propNew1.id

      }
      "do not add dublicate properties" in {
        val ent = getEmptyEntity

        val multiAllDuplicates = ent.and(Seq(propNew1, propNewDuplicate1))
        val varargAllDuplicates = ent.and(propNew1, propNewDuplicate1)

        multiAllDuplicates.memory shouldBe emptyMemory
        varargAllDuplicates.memory shouldBe emptyMemory

        multiAllDuplicates.ids shouldBe Set.empty
        varargAllDuplicates.ids shouldBe Set.empty


        val multiMixedDuplicates = ent.and(Seq(propNew1, propNewDuplicate1, propNew2))
        val varargMixedDuplicates = ent.and(propNew1, propNewDuplicate1, propNew2)

        multiMixedDuplicates.memory shouldBe Seq(PropertyAdded(propNew2))
        varargMixedDuplicates.memory shouldBe Seq(PropertyAdded(propNew2))

        multiMixedDuplicates.ids shouldBe Set(propNew2.id)
        varargMixedDuplicates.ids shouldBe Set(propNew2.id)
      }
    }
    "changeable" must {
      "accept only properly typed properties" in {
        "StatelessEntity.empty[Int].change(propString)" shouldNot typeCheck
      }
      "change existent properties" in {

        val ent = getFilledEntity

        val single = ent.change(propInStateChanges1)
        val multi = ent.change(Seq(propInStateChanges1, propInStateChanges2))
        val vararg = ent.change(propInStateChanges1, propInStateChanges2, propInStateChanges3)

        single.memory shouldBe PropertyChanged(propInStateChanges1) +: initialMemory
        multi.memory shouldBe
          Seq(PropertyChanged(propInStateChanges2), PropertyChanged(propInStateChanges1)) ++: initialMemory
        vararg.memory shouldBe
          Seq(PropertyChanged(propInStateChanges3), PropertyChanged(propInStateChanges2),
            PropertyChanged(propInStateChanges1)) ++: initialMemory
      }
      "do not change anything given non existent properties" in {
        val ent = getEmptyEntity

        val singleNew = ent.change(propNew1)
        val multiNew = ent.change(Seq(propInStateChanges1, propInStateChanges2))
        val varargNew = ent.change(propInStateChanges1, propInStateChanges2, propInStateChanges3)


        singleNew.memory shouldBe emptyMemory
        multiNew.memory shouldBe emptyMemory
        varargNew.memory shouldBe emptyMemory
      }
      "do not change dublicate properties" in {
        val ent = getFilledEntity

        val multiAllDuplicates = ent.change(Seq(propNew1, propNewDuplicate1))
        val varargAllDuplicates = ent.change(propNew1, propNewDuplicate1)

        multiAllDuplicates.memory shouldBe initialMemory
        varargAllDuplicates.memory shouldBe initialMemory

        val multiMixedDuplicates = ent.change(Seq(propNew1, propNewDuplicate1, propInStateChanges2))
        val varargMixedDuplicates = ent.change(propNew1, propNewDuplicate1, propInStateChanges2)

        multiMixedDuplicates.memory shouldBe PropertyChanged(propInStateChanges2) +: initialMemory
        varargMixedDuplicates.memory shouldBe PropertyChanged(propInStateChanges2) +: initialMemory
      }

    }

    "transactional" must {
      import EnableTransactions._
      "accept transaction" in {
        val ent = getEmptyEntity
        val trans = SimpleTransaction(AddProperty(propNew1), ChangeProperty(propNewDuplicate1))
        "ent.transaction(SimpleTransaction(AddProperty(propNew1), " +
          "ChangeProperty(propNewDuplicate1)))" should compile
        "ent.transaction(trans)" should compile
      }
      "perform valid transaction" in {
        val ent = getEmptyEntity
        val trans = SimpleTransaction(AddProperty(propNew1), AddProperty(propNew2))
        val result = ent.transaction(trans)

        result.memory shouldBe Seq(PropertyAdded(propNew2), PropertyAdded(propNew1))
        result.ids shouldBe Set(propNew1.id, propNew2.id)

      }
      "perform transactions with sequential commands on one property" in {
        val ent = getEmptyEntity

        val result = ent.transaction(SimpleTransaction(AddProperty(propNew1), ChangeProperty(propNewDuplicate1)))

        result.memory shouldBe Seq(PropertyChanged(propNewDuplicate1), PropertyAdded(propNew1))
        result.ids shouldBe Set(propNew1.id)
      }
      "do not perform invalid transactions" in {
        val ent = getFilledEntity
        val allCmdsInvalid =
          ent.transaction(SimpleTransaction(AddProperty(propInStateChanges1), AddProperty(propInStateChanges2)))

        allCmdsInvalid.memory shouldBe initialMemory
        allCmdsInvalid.ids shouldBe initialIds

        val someCmdsInvalid =
          ent.transaction(SimpleTransaction(ChangeProperty(propInStateChanges1), ChangeProperty(propNew1)))

        someCmdsInvalid.memory shouldBe initialMemory
        allCmdsInvalid.ids shouldBe initialIds

      }
    }
  }
  "EntityView" when {
    import EntityView.Ops._

    val getEmptyEntity = EntityView.empty[Int]()
    val getFilledEntity = EntityView.empty[Int]().and(propInState1, propInState2, propInState3)

    "extensible" must {
      "accept only properly typed properties" in {
        "EntityView.empty[Int]().add(propString)" shouldNot typeCheck
      }
      "add properties" in {
        val ent = getEmptyEntity
        val result = ent.and(propNew1, propNew2, propNew3)

        result.state shouldBe Map(propNew1.id -> propNew1, propNew2.id -> propNew2, propNew3.id -> propNew3)
      }
    }
    "changeable" must {
      "accept only properly typed properties" in {
        "EntityView.empty[Int]().change(propString)" shouldNot typeCheck
      }
      "change properties" in {
        val ent = getFilledEntity
        val result = ent.change(propInStateChanges1, propInStateChanges2, propInStateChanges3)

        result.state shouldBe Map(
          propInState1.id -> propInStateChanges1,
          propInState2.id -> propInStateChanges2,
          propInState3.id -> propInStateChanges3)
      }
    }
  }
}
