package io.prods.engine.core.test.share

import io.prods.engine.core.entity.property.ValueTypes.{Currency, Text}

trait MockTypeclassProperties {


  import io.prods.engine.core.entity.property.properties._


  val prop = NamedProperty(1, Text("First one!"), Text("one"), 1)
  val prop2 = NamelessProperty(2, Text("No name :("), 1)
  val prop3 = Named(3, Text("Bronze"), Text("three"), 1)
  val prop3newValue = Named(3, Text("Third"), Text("three"), 1)
  val propString = NamedProperty("str", Text("Well, fuck..."), Text("String, man"), 1)

  val propInState1 = SimpleProperty(1, Text("Original 1"))
  val propInState2 = SimpleProperty(2, Text("Original 2"))
  val propInState3 = SimpleProperty(3, Text("Original 3"))
  val propInStateChanges1 = SimpleProperty(1, Text("Changed 1"))
  val propInStateChanges2 = SimpleProperty(2, Text("Changed 2"))
  val propInStateChanges3 = SimpleProperty(3, Text("Changed 3"))
  val propNew1 = SimpleProperty(4, Text("New 4"))
  val propNew2 = SimpleProperty(5, Text("New 5"))
  val propNew3 = SimpleProperty(6, Text("New 6"))
  val propNewDuplicate1 = SimpleProperty(4, Text("Duplicate 4"))
  val propNewDuplicate2 = SimpleProperty(5, Text("Duplicate 5"))
  val propNewDuplicate3 = SimpleProperty(6, Text("Duplicate 6"))
  val newTypeProp1 = SimpleProperty(1, Currency(1))
  val newTypeProp2 = SimpleProperty(2, Currency(2))
  val newTypeProp3 = SimpleProperty(3, Currency(3))
}
