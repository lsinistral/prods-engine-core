import java.net.URL

import sbt.Keys._
import sbt._

import Versions._

object Dependencies {
  lazy val commonSettings = Seq(
    organization := "io.prods",
    scalaVersion := "2.11.8",
    resolvers ++= resolvrs
  )
  val resolvrs = Seq(
    Resolver.url("Typesafe repository releases", new URL("http://repo.typesafe.com/typesafe/releases/")),
    Resolver.url("dnvriend at bintray", new URL("http://dl.bintray.com/dnvriend/maven/"))
  )


  val testLib = Seq(
    "org.scalatest" %% "scalatest" % scalatestVer % "test"
  )


  val logging = Seq(
    "org.slf4j" % "slf4j-api" % slf4jVer,
    "org.slf4j" % "slf4j-simple" % slf4jVer
  )

  val akkaGroup = "com.typesafe.akka"


  val akka = Seq(
    akkaGroup %% "akka-actor" % akkaVer,
    akkaGroup %% "akka-testkit" % akkaVer % "test",
    akkaGroup %% "akka-slf4j" % akkaVer
  )

  val akkaPersistence = Seq(
    akkaGroup %% "akka-persistence" % akkaVer,
    "com.github.dnvriend" %% "akka-persistence-inmemory" % "1.1.4" % "test",
    "com.github.scullxbones" %% "akka-persistence-mongo-rxmongo" % "1.2.1"
  )


  val mongo = Seq(
    "org.reactivemongo" %% "reactivemongo" % reactiveMongoVer
  )
}