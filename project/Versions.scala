object Versions {
  //prods core version
  val coreVer = "0.2.0"

  //dependency versions
  val scalatestVer = "2.2.6"
  val slf4jVer = "1.7.19"
  val akkaVer = "2.4.2"
  val reactiveMongoVer = "0.11.10"
}