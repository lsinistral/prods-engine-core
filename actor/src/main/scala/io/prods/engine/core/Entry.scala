package io.prods.engine.core

import akka.actor.ActorSystem
import io.prods.engine.core.management._


class Entry {
  val coreSystem = ActorSystem("prods-engine-core")

  val storage = coreSystem.actorOf(PersistentWarehouseStorage.props)
  val managerRouter = coreSystem.actorOf(ActorManager.props)

  val warehouse = coreSystem.actorOf(ActorWarehouse.props(storage, managerRouter))

}
