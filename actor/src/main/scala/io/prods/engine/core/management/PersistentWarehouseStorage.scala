package io.prods.engine.core.management

import akka.actor.{ActorRef, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.storage.DeletableItemsStore.DeletableEntitiesStoreOps
import io.prods.engine.core.storage.SimpleStorage
import io.prods.engine.core.storage.SimpleStorage.{SimpleStorageCanStoreMapDelete, SimpleStorageCanStoreMaps}
import io.prods.engine.core.storage.Store.StorageOps


object PersistentWarehouseStorage {

  implicit object Store extends SimpleStorageCanStoreMaps[EntityId, Option[ActorRef]] with
                                SimpleStorageCanStoreMapDelete[EntityId, Option[ActorRef]]

  def props = Props(new PersistentWarehouseStorage(new SimpleStorage(Map.empty[EntityId, Option[ActorRef]]),
    "persistence-warehouse"))

  def testProps(storage: SimpleStorage[Map[EntityId, Option[ActorRef]]], id: String) =
    Props(new PersistentWarehouseStorage(storage, id))

  sealed trait Cmd extends ActorCmd

  sealed trait Reply extends ActorReply

  sealed trait Evt extends PersistentEvent

  object commands {

    final case class CheckEntityExistence(id: EntityId) extends Cmd

    final case class StoreEntity(id: EntityId, ref: ActorRef) extends Cmd

    final case class DisableEntity(id: EntityId) extends Cmd

  }

  object replies {

    sealed trait PresenceReply extends Reply

    final case class Present(id: EntityId, ref: ActorRef) extends PresenceReply

    final case class PresentNoActor(id: EntityId) extends PresenceReply

    final case class NotPresent(id: EntityId) extends PresenceReply


    case object Done extends Reply

    case object StorageError extends Reply

  }

  object events {

    final case class EntityAdded(entity: EntityId) extends Evt

    final case class EntityDisabled(entity: EntityId) extends Evt

  }

}


class PersistentWarehouseStorage(st: SimpleStorage[Map[EntityId, Option[ActorRef]]], id: String) extends
  PersistentActor {


  import PersistentWarehouseStorage._
  import PersistentWarehouseStorage.commands._
  import PersistentWarehouseStorage.replies._


  def persistenceId: String = id

  var pendingChanges = 0

  def saveTrigger(): Unit = {
    pendingChanges = pendingChanges + 1
    if (pendingChanges >= 1000) {
      pendingChanges = 0
      saveSnapshot(storage.items.keySet)
    }
  }

  var storage = st

  def receiveCommand = {
    case CheckEntityExistence(id: EntityId) =>
      val presence = storage get id match {
        case Some((_, Some(ref))) => Present(id, ref)

        case Some((_, None)) => PresentNoActor(id)

        case None => NotPresent(id)
      }
      sender() ! presence

    case StoreEntity(ident, ref) =>
      val originalSender = sender()
      storage get ident match {
        case None =>
          persist(events.EntityAdded(ident))(evt => {
            storage = storage add ident -> Some(ref)
            originalSender ! Done
            saveTrigger()
          })

        case _ => originalSender ! StorageError
      }

    case DisableEntity(ident) =>
      val originalSender = sender()
      storage get ident match {
        case Some(item: (EntityId, Option[ActorRef])) =>
          persist(events.EntityDisabled(ident))(evt => {
            storage = storage delete Set(ident)
            originalSender ! Done
            saveTrigger()
          })

        case _ =>
          originalSender ! StorageError
      }

    case _ => sender() ! UnsupportedOperation
  }

  def receiveRecover: Receive = {
    case events.EntityAdded(ident) => storage = storage add ident -> None

    case events.EntityDisabled(ident) => storage = storage delete Set(ident)

    case SnapshotOffer(_, snapshot: Set[EntityId@unchecked]) if snapshot.head.isInstanceOf[EntityId] =>
      storage =
        SimpleStorage(snapshot.map(id => id -> None)
          .toMap[EntityId, Option[ActorRef]])
      println(snapshot)
    case SnapshotOffer(_, s) => println(s)
  }

}

