package io.prods.engine.core.management

import akka.actor.{Actor, ActorRef, Props}
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.management.Collector.commands._
import io.prods.engine.core.management.Collector.replies._

import scala.reflect.ClassTag


/**
  * This is Collector actor.
  * It&#96;s job is to collect multiple items of one type combine them to sequence and send back to concerned actor.
  * Collector is not responsible for creation of worker actors.
  *
  * While collecting Collector accepts Collect command, it must contain item of proper type.
  * When collecting is done Collector switches to collected mode, sends itself FinishedCollecting command
  * and accepts no more Collect commands. Then it sends Collected reply with all collected items to concerned actor
  * and shuts itself down.
  *
  * It is possible to abort collection of items by passing AbortCollecting command to Collector. Then collector will
  * send
  * PartiallyCollected reply with collected items to concerned actor and shuts itself down
  *
  * @param id             collector unique number
  * @param itemsToCollect number of items to collect
  * @param reportTo       actor ref of concerned actor
  * @tparam T item type
  */
class Collector[T: ClassTag](val id: Int, var itemsToCollect: Int, reportTo: ActorRef) extends Actor {
  var items: Seq[T] = Seq.empty[T]

  def receive: Receive = collecting

  /**
    * Collects item.
    *
    * @param item item to be collected
    */
  def collect(item: T) = {
    items = item +: items
    if (itemsToCollect > 1) itemsToCollect = itemsToCollect - 1
    else {
      context.become(collected)
      self ! FinishedCollecting
    }

  }

  /**
    * @return behavior for collecting stage
    */
  def collecting: Receive = {
    case Collect(item: T) => collect(item)

    case item: T => collect(item)

    case Collect(_) => sender() ! WrongType

    case AbortCollecting => report(PartiallyCollected(items))

    case _ => sender() ! WrongType
  }

  /**
    * @return behavior for collected stage
    */
  def collected: Receive = {
    case FinishedCollecting => report(Collected(items, id))

    case Collect(_) => sender() ! AllCollected

    case _ => sender() ! AllCollected
  }

  /**
    * @param reply Reply send to concerned actor. Collected or PartiallyCollected mostly.
    */
  private def report(reply: Reply) = {
    reportTo ! reply
    context.stop(self)
  }
}

object Collector {

  /**
    * Props factory for Collector.
    * Creates new props.
    *
    * @param itemsToCollect number of items to collect
    * @param reportTo       actor ref of concerned actor
    * @param id             collector unique number, non obligatory, default value 0
    * @tparam T item type
    *
    * @return new shiny Props for Collector
    */
  def props[T: ClassTag](itemsToCollect: Int, reportTo: ActorRef, id: Int = 0) = Props(new Collector[T](id,
    itemsToCollect, reportTo))

  object commands {

    /**
      * Base trait for Collector commands
      */
    sealed trait Cmd extends ActorCmd

    /**
      * Collect command used for passing items to Collector instance. Item should be properly typed,
      * otherwise Collector reject it with WrongType reply
      *
      * @param item item to collect
      * @tparam T item type
      */
    final case class Collect[T](item: T) extends Cmd

    /**
      * AbortCollecting command used for immediately abort collecting of items, report to concerned actor and shutdown
      */
    case object AbortCollecting extends Cmd

    /**
      * FinishedCollecting is internal command passed by Collector to itself when all items are collected
      */
    private[Collector] case object FinishedCollecting extends Cmd

  }

  object replies {

    /**
      * Base trait for Collector replies
      */
    sealed trait Reply extends ActorReply

    /**
      * Reply with all collected items
      * Send to concerned (reportTo) actor
      *
      * @param items all collected items
      * @param id    unique Collector number, defaults to 0
      * @tparam T item type
      */
    final case class Collected[T](items: T, id: Int = 0) extends Reply

    final case class PartiallyCollected[T](items: T) extends Reply

    /**
      * Reply for Collect command with wrong item type
      */
    case object WrongType extends Reply

    /**
      * Reply for Collect command when all items are collected
      */
    case object AllCollected extends Reply

  }

}
