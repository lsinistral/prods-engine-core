package io.prods.engine.core.entity

import akka.actor.{ActorRef, Props}
import akka.persistence.{PersistentActor, SnapshotOffer}
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.entity.GenericPersistentProduct.Reply
import io.prods.engine.core.entity.GenericPersistentProduct.commands._
import io.prods.engine.core.entity.GenericPersistentProduct.reply.EntityChange
import io.prods.engine.core.entity.PersistentProduct.commands._
import io.prods.engine.core.entity.property.KeyProp
import io.prods.engine.core.product.StatelessProduct

import scala.collection.mutable


object GenericPersistentProduct {


  def props(id: EntityId) = Props(new PersistentProduct(id))

  sealed trait Cmd extends ActorCmd

  sealed trait Reply extends ActorReply

  object commands {

    case object Save extends Cmd

    case object Shutdown extends Cmd

    case object GetIdAsString extends Cmd

    case object GetId extends Cmd

    case object Subscribe extends Cmd

    final case class GetHistory(subscribe: Boolean = false) extends Cmd

    case object CreateVariant

  }

  object reply {

    case class EntityChange(evt: EntityEvent) extends Reply

    case class EntityChanges(evt: Seq[EntityEvent]) extends Reply

  }

}

abstract class AbsPersistentProduct[K](val id: EntityId) extends PersistentActor {

  def this(ent: StatelessProduct[K]) = {
    this(ent.id)
    entity = ent
  }

  override def preStart(): Unit = {
    if (entity.nonEmpty) saveSnapshot(entity)
  }

  def persistenceId: String = id.toString

  var entity = StatelessProduct[K](id = id)

  val subscribers = mutable.ListBuffer.empty[ActorRef]

  def notifySubscribers(payload: Reply) = subscribers.foreach(_ ! payload)

  def receiveRecoverSnapshot: Receive = {
    case SnapshotOffer(m, s: StatelessProduct[K]) => entity = s
  }

  def unsupportedOp: Receive = {
    case _ => sender() ! UnsupportedOperation
  }

  def receiveRecoverProp: Receive

  def receiveRecover: Receive = receiveRecoverSnapshot orElse receiveRecoverProp orElse unsupportedOp


  def receiveServiceCommand: Receive = {
    case Save =>
      saveSnapshot(entity)

    case Shutdown => context.stop(self)

    case GetIdAsString => sender() ! persistenceId

    case GetId => sender() ! id

    case GetHistory(subscribe) => sender() ! entity.history

    case Subscribe =>
      val snd = sender()
      subscribers += snd

  }

  def receiveCommand: Receive = receivePropCommand orElse receiveServiceCommand orElse unsupportedOp

  def receivePropCommand: Receive

}

class PersistentProduct(id: EntityId) extends AbsPersistentProduct[Int](id) {


  import io.prods.engine.core.entity.Entity.UnsafeOps._
  import io.prods.engine.core.entity.Entity.Util._
  import io.prods.engine.core.entity.Entity.ValidationOps._


  def this(ent: StatelessProduct[Int]) = {
    this(ent.id)
    entity = ent
  }


  def receiveRecoverProp: Receive = {
    case PropertyAdded(prop: KeyProp[Int]@unchecked) =>
      entity = entity and prop
    case PropertyChanged(prop: KeyProp[Int]@unchecked) =>
      entity = entity change prop
  }

  def receivePropCommand: Receive = {
    case AddProperties(props) =>
      props.foreach(context.self ! AddProperty(_))

    case ChangeProperties(props) =>
      props.foreach(context.self ! ChangeProperty(_))

    case AddProperty(prop) =>
      entity.validateAddition(prop).valid
        .foreach(prop => {
          val evt = PropertyAdded(prop)
          persist(evt) { e =>
            subscribers.foreach(_ ! EntityChange(e))
            entity = entity and prop
          }
        })

    case ChangeProperty(prop) =>
      entity.validateChanges(prop).valid
        .foreach(prop => {
          val evt = PropertyChanged(prop)
          persist(evt) { e =>
            subscribers.foreach(_ ! EntityChange(e))
            entity = entity change prop
          }
        })
  }
}


object PersistentProduct {
  def props(ent: StatelessProduct[Int]) = Props(new PersistentProduct(ent))

  object commands {

    final case class AddProperty(property: KeyProp[Int])

    final case class ChangeProperty(property: KeyProp[Int])

    final case class AddProperties(properties: Set[KeyProp[Int]])

    final case class ChangeProperties(properties: Set[KeyProp[Int]])

  }

}