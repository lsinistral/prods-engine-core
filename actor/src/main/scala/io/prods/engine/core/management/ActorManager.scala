package io.prods.engine.core.management

import akka.actor.{Actor, ActorRef, Props}
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.entity.GenericPersistentProduct
import io.prods.engine.core.management.ActorManager.commands.{CreateEntity, TerminateEntity}
import io.prods.engine.core.management.ActorManager.reply.EntityCreated


class ActorManager extends Actor {
  def receive: Receive = {
    case CreateEntity(id) =>
      sender() ! EntityCreated(id, context.actorOf(GenericPersistentProduct.props(id), id.toString))
    case TerminateEntity(id) =>
      context.child(id.toString).foreach(context.stop)
    case _ => sender() ! UnsupportedOperation
  }
}

object ActorManager {

  def props = Props(new ActorManager)

  object commands {

    sealed trait Cmd extends ActorCmd

    final case class CreateEntity(id: EntityId) extends Cmd

    final case class TerminateEntity(id: EntityId) extends Cmd

  }

  sealed trait Reply extends ActorReply {
    val id: EntityId
    val ref: ActorRef
  }

  object reply {

    final case class EntityCreated(id: EntityId, ref: ActorRef) extends Reply

    final case class EntityFound(id: EntityId, ref: ActorRef) extends Reply

  }

}
