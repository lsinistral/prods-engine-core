package io.prods.engine.core.management.warehouse

import akka.actor.{ActorRef, FSM, Props}
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.management.ActorManager.commands.CreateEntity
import io.prods.engine.core.management.ActorManager.reply.EntityCreated
import io.prods.engine.core.management.ActorWarehouse.commands.GetEntities
import io.prods.engine.core.management.ActorWarehouse.replies.{EntitiesFound, EntitiesNotFound}
import io.prods.engine.core.management.Collector.replies.Collected
import io.prods.engine.core.management.PersistentWarehouseStorage.commands.CheckEntityExistence
import io.prods.engine.core.management.PersistentWarehouseStorage.replies._
import io.prods.engine.core.management._
import io.prods.engine.core.management.warehouse.GettingWorker._

import scala.concurrent.duration._


object GettingWorker {
  def props(storage: ActorRef, managerRouter: ActorRef) =
    Props(new GettingWorker(storage, managerRouter))


  case class StateData(client: ActorRef = ActorRef.noSender,
                       entities: EntDict = Map.empty)


  case object Flush

  sealed trait State extends FsmActorState

  case object Listening extends State

  case object WaitingForCollectorResponse extends State

  case object WaitingForNewEntities extends State

  case object WaitingForStorageResponse extends State

  case object WaitingForManagerResponse extends State

}


class GettingWorker(storage: ActorRef, managerRouter: ActorRef, val timeout: FiniteDuration = 1.second) extends
  FSM[State, StateData] {

  startWith(Listening, StateData())

  when(Listening) {
    case Event(GetEntities(ids), _) =>
      val originalSender = sender()
      val nextStep = if (ids.size == 1) {
        storage ! CheckEntityExistence(ids.head)
        WaitingForStorageResponse
      }
      else {
        val collector = context.actorOf(Collector.props[PresenceReply](ids.size, self))
        ids.foreach(id => storage.tell(CheckEntityExistence(id), collector))
        WaitingForCollectorResponse
      }

      goto(nextStep) using StateData(originalSender)
  }

  when(WaitingForStorageResponse, stateTimeout = timeout) {
    case Event(Present(id, ref), StateData(client, _)) =>
      client ! EntitiesFound(Map(id -> ref))

      goto(Listening) using StateData()

    case Event(PresentNoActor(id), _) =>
      managerRouter ! CreateEntity(id)

      goto(WaitingForManagerResponse)

    case Event(NotPresent(_), StateData(client, _)) =>
      client ! EntitiesNotFound

      goto(Listening) using StateData()
  }

  when(WaitingForCollectorResponse, stateTimeout = timeout) {
    case Event(
    Collected(items: Seq[PresenceReply]@unchecked, _), data) if items.head.isInstanceOf[PresenceReply] =>
      import scala.collection.mutable

      val fullPresence = mutable.Map.empty[EntityId, ActorRef]
      val idOnly = mutable.ListBuffer.empty[EntityId]
      items.foreach {
        case Present(id, ref) => fullPresence += id -> ref

        case PresentNoActor(id) => idOnly += id

        case NotPresent(_) => ()
      }

      val newState = if (fullPresence.nonEmpty) data.copy(entities = fullPresence.toMap)
      else data

      if (idOnly.nonEmpty) {
        val collector = context.actorOf(Collector.props[EntityCreated](idOnly.size, self))

        idOnly.foreach(id => managerRouter.tell(CreateEntity(id), collector))

        goto(WaitingForNewEntities) using newState
      }
      else {
        newState.client ! EntitiesFound(newState.entities)

        goto(Listening) using StateData()
      }
  }

  when(WaitingForManagerResponse, stateTimeout = timeout) {
    case Event(EntityCreated(id, ref), data) =>
      data.client ! EntitiesFound(Map(id -> ref))

      goto(Listening) using StateData()
  }

  when(WaitingForNewEntities, stateTimeout = timeout) {
    case Event(
    Collected(items: Seq[EntityCreated]@unchecked, _), data) if items.head.isInstanceOf[EntityCreated] =>
      val newEntities: Map[EntityId, ActorRef] = items.map(entity => entity.id -> entity.ref).toMap

      data.client ! EntitiesFound(data.entities ++ newEntities)

      goto(Listening) using StateData()
  }

  whenUnhandled {
    case Event(StateTimeout, StateData(client, _)) =>
      client ! EntitiesNotFound

      goto(Listening) using StateData()

    case _ => sender() ! UnsupportedOperation

      stay
  }
}
