package io.prods.engine.core

import akka.actor.ActorRef


object actorCommons {

  type EntDict = Map[EntityId, ActorRef]

  /**
    * Marker base traits for all actor&#96;s replies
    * and commands
    */
  trait ActorMsg

  trait ActorReply extends ActorMsg

  trait ActorCmd extends ActorMsg

  trait PersistentEvent

  trait FsmActorState

  /**
    * Actor error reply for illegal commands passed to that actor
    */
  case object UnsupportedOperation extends ActorReply

  /**
    * Actor command for testing and ping functionality
    * replies intended: Pong, UnsupportedOperation
    */
  case object Ping extends ActorCmd

  /**
    * Actor reply for Ping command
    */
  case object Pong extends ActorReply


}
