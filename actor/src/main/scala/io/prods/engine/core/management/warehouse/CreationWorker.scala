package io.prods.engine.core.management.warehouse

import akka.actor.{ActorRef, FSM, Props}
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._
import io.prods.engine.core.management.ActorManager.commands.CreateEntity
import io.prods.engine.core.management.ActorManager.reply.EntityCreated
import io.prods.engine.core.management.ActorWarehouse.commands.CreateEntities
import io.prods.engine.core.management.ActorWarehouse.replies.{EntitiesCreated, EntitiesNotCreated}
import io.prods.engine.core.management.Collector
import io.prods.engine.core.management.Collector.replies.Collected
import io.prods.engine.core.management.PersistentWarehouseStorage.commands.StoreEntity
import io.prods.engine.core.management.PersistentWarehouseStorage.replies._
import io.prods.engine.core.management.warehouse.CreationWorker._

import scala.concurrent.duration._


object CreationWorker {
  def props(storage: ActorRef, managerRouter: ActorRef) =
    Props(new CreationWorker(storage, managerRouter))

  def props(storage: ActorRef, managerRouter: ActorRef, timeout: FiniteDuration) =
    Props(new CreationWorker(storage, managerRouter, timeout))


  final case class StateData(client: ActorRef = ActorRef.noSender,
                             entities: EntDict = Map.empty,
                             itemsToGo: Int = 0)


  sealed trait State extends FsmActorState

  case object Listening extends State

  case object WaitingForCollectorResponse extends State

  case object WaitingForStorageResponse extends State

  case object WaitingForManagerResponse extends State

}

class CreationWorker(storage: ActorRef, managerRouter: ActorRef, val timeout: FiniteDuration = 1.second) extends
  FSM[State, StateData] {

  startWith(Listening, StateData())

  when(Listening) {
    case Event(CreateEntities(count), _) =>
      val originalSender = sender()

      val nextStep = if (count == 1) {
        managerRouter ! CreateEntity(generateID)
        WaitingForManagerResponse
      }
      else {
        val collector = context.actorOf(
          Collector.props[EntityCreated](count, self))
        (1 to count).foreach(i => managerRouter.tell(CreateEntity(generateID), collector))
        WaitingForCollectorResponse
      }

      goto(nextStep) using StateData(originalSender, Map.empty, count)
  }

  when(WaitingForManagerResponse, stateTimeout = timeout) {
    case Event(EntityCreated(id, ref), data) =>
      storage ! StoreEntity(id, ref)
      goto(WaitingForStorageResponse) using data.copy(entities = Map(id -> ref))
  }

  when(WaitingForCollectorResponse, stateTimeout = timeout) {
    case Event(
    Collected(items: Seq[EntityCreated]@unchecked, _), data) if items.head.isInstanceOf[EntityCreated] =>
      val newEntities = items.map(entity => {
        storage ! StoreEntity(entity.id, entity.ref)
        entity.id -> entity.ref
      }).toMap

      goto(WaitingForStorageResponse) using data.copy(entities = newEntities)
  }

  when(WaitingForStorageResponse, stateTimeout = timeout) {
    case Event(Done, data) =>
      if (data.itemsToGo > 1)
        stay using data.copy(itemsToGo = data.itemsToGo - 1)
      else {
        data.client ! EntitiesCreated(data.entities)
        goto(Listening) using StateData()
      }

    case Event(StorageError, StateData(client, _, _)) =>
      client ! EntitiesNotCreated

      goto(Listening) using StateData()
  }

  whenUnhandled {
    case Event(StateTimeout, StateData(client, _, _)) =>
      client ! EntitiesNotCreated
      goto(Listening) using StateData()
    case _ => sender() ! UnsupportedOperation
      stay
  }

}
