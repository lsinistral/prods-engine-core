package io.prods.engine.core.management

import akka.actor._
import io.prods.engine.core._
import io.prods.engine.core.actorCommons._


object ActorWarehouse {
  def props(system: ActorSystem) =
    Props(new ActorWarehouse(system.actorOf(PersistentWarehouseStorage.props), system.actorOf(ActorManager
      .props)))

  def testProps(system: ActorSystem) =
    Props(new ActorWarehouse(system.actorOf(PersistentWarehouseStorage.props), system.actorOf(ActorManager.props)))

  def props(storage: ActorRef, managerRouter: ActorRef) = Props(new ActorWarehouse(storage, managerRouter))

  sealed trait Cmd extends ActorCmd

  sealed trait Reply extends ActorReply

  object commands {

    final case class CreateEntities(count: Int = 1) extends Cmd

    final case class GetEntities(ids: Set[EntityId]) extends Cmd

    case object GetAllEntities extends Cmd

    final case class DisableEntities(ids: Set[EntityId]) extends Cmd

  }

  object replies {

    final case class EntitiesFound(entities: Map[EntityId, ActorRef]) extends Reply

    case object EntitiesNotFound extends Reply

    final case class EntitiesCreated(entities: Map[EntityId, ActorRef]) extends Reply

    case object EntitiesNotCreated extends Reply

  }

}


class ActorWarehouse(storage: ActorRef, managerRouter: ActorRef) extends Actor {


  import ActorWarehouse.commands._


  def receive = {
    case m@CreateEntities(_) =>
      context.actorOf(warehouse.CreationWorker.props(storage, managerRouter)) forward m
    case m@GetEntities(_) =>
      context.actorOf(warehouse.GettingWorker.props(storage, managerRouter)) forward m
    case GetAllEntities =>
    case DisableEntities(ids) =>
    case _ => sender() ! UnsupportedOperation
  }

}









