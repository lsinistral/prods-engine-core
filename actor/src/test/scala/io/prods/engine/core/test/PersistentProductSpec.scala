package io.prods.engine.core.test

import akka.actor.{Props, ActorRef}
import akka.testkit.TestProbe
import io.prods.engine.core._
import io.prods.engine.core.entity.GenericPersistentProduct.commands._
import io.prods.engine.core.entity.GenericPersistentProduct.reply.EntityChange
import io.prods.engine.core.entity.PersistentProduct.commands._
import io.prods.engine.core.entity._
import io.prods.engine.core.entity.component.EntityHistory
import io.prods.engine.core.product.StatelessProduct
import io.prods.engine.core.test.share.MockTypeclassProperties
import org.scalatest.WordSpecLike

import scala.concurrent.duration._
import Entity.Ops._
import StatelessProduct._


trait PersistentProductBehavior extends ActorAsyncSpec with MockTypeclassProperties {
  this: WordSpecLike =>

  val initialMemory = Seq(PropertyAdded(propInState3), PropertyAdded(propInState2), PropertyAdded(propInState1))

  val initialIds = Set(propInState1.id, propInState2.id, propInState3.id)

  val changedMemory = Seq(
    PropertyChanged(propInStateChanges2),
    PropertyChanged(propInStateChanges1),
    PropertyAdded(propInState3),
    PropertyAdded(propInState2),
    PropertyAdded(propInState1))

  def getActor(id: EntityId = generateID) = system.actorOf(GenericPersistentProduct.props(id), id.toString)

  def getActor(ent: StatelessProduct[Int]) = system.actorOf(PersistentProduct.props(ent), ent.id.toString)

  def returnIds() = {
    val id = generateID
    val ref = getActor(id)

    "return id" in {
      within(100.millis) {
        ref ! GetId
        expectMsgType[EntityId] shouldBe id
      }
    }
    "return string id representation" in {
      within(100.millis) {
        ref ! GetIdAsString
        expectMsgType[String] shouldBe id.toString
      }
    }
  }

  def addProperties(givenRef: => ActorRef) = {
    "add one property" in {
      val ref = getActor()
      within(100.millis) {
        ref ! AddProperty(propInState1)
        expectNoMsg
      }

      within(100.millis) {
        ref ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe Seq(PropertyAdded(propInState1))
        history.ids shouldBe Set(propInState1.id)
      }
    }
    "add properties" in {
      val ref = getActor()

      within(100.millis) {
        ref ! AddProperties(Set(propInState1, propInState2, propInState3))
        expectNoMsg
      }

      within(100.millis) {
        ref ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe initialMemory
        history.ids shouldBe initialIds
      }
    }
    "do not add invalid properties" in {
      val ent = StatelessProduct[Int]().and(propInState1, propInState2, propInState3)
      val ref = getActor(ent)

      within(100.millis) {
        ref ! AddProperty(propInStateChanges1)
        expectNoMsg
      }
      within(100.millis) {
        ref ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe initialMemory
        history.ids shouldBe initialIds
      }
    }
    "persist addition" in {
      val id = generateID
      val ref = getActor(id)
      within(100.millis) {
        ref ! AddProperties(Set(propInState1, propInState2, propInState3))
        expectNoMsg
      }
      within(10.millis) {
        ref ! Shutdown
        expectNoMsg
      }

      val ref2 = getActor(id)
      within(100.millis) {
        ref2 ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe initialMemory
        history.ids shouldBe initialIds
      }
    }
  }

  def changeProperties(givenRef: => ActorRef) = {
    "change one property" in {
      val ref = getActor()
      within(100.millis) {
        ref ! AddProperty(propInState1)
        ref ! ChangeProperty(propInStateChanges1)
        expectNoMsg
      }

      within(100.millis) {
        ref ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe Seq(
          PropertyChanged(propInStateChanges1),
          PropertyAdded(propInState1)
        )
        history.ids shouldBe Set(1)
      }
    }

    "change properties" in {
      val ent = StatelessProduct[Int]().and(propInState1, propInState2, propInState3)
      val ref = getActor(ent)
      within(100.millis) {
        ref ! ChangeProperties(Set(propInStateChanges1, propInStateChanges2))
        expectNoMsg
      }

      within(100.millis) {
        ref ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe changedMemory
        history.ids shouldBe initialIds
      }
    }
    "do not change invalid properties" in {
      val ref = getActor()

      within(100.millis) {
        ref ! ChangeProperty(propNew1)
        expectNoMsg
      }
    }
    "persist changes" in {
      val id = generateID
      val ent = StatelessProduct[Int](id = id).and(propInState1, propInState2, propInState3)
      val ref = getActor(ent)
      within(100.millis) {
        ref ! AddProperties(Set(propInState1, propInState2, propInState3))
        expectNoMsg
        ref ! ChangeProperties(Set(propInStateChanges1, propInStateChanges2))
        expectNoMsg
      }
      within(10.millis) {
        ref ! Shutdown
        expectNoMsg
      }

      val ref2 = getActor(id)
      within(100.millis) {
        ref2 ! GetHistory()
        val history = expectMsgType[EntityHistory[Int]]
        history.memory shouldBe changedMemory
        history.ids shouldBe initialIds
      }
    }
  }
}


class PersistentProductSpec extends ActorAsyncSpec with MockTypeclassProperties with PersistentProductBehavior {
  "Persistent Product" when {
    behave like returnIds()
    "initializing" must {
      "accept existent entity" in {
        import Entity.Ops._
        import StatelessProduct._
        val ent = StatelessProduct.empty[Int].and(propInState1, propInState2, propInState3)
        val ref = system.actorOf(Props(new PersistentProduct(ent)))

        within(100.millis) {
          ref ! GetHistory()
          val history = expectMsgType[EntityHistory[Int]]
          history.memory shouldBe initialMemory
        }

      }
    }
    "accepts property commands" must {
      behave like addProperties(getActor())
      behave like changeProperties(getActor())
    }
    "subscribe peers to events and notify them" in {
      val probe = TestProbe()
      val ref = getActor()
      ref.tell(Subscribe, probe.ref)

      within(100.millis) {
        ref ! AddProperty(propNew1)

        val change = probe.expectMsgType[EntityChange]
        change.evt shouldBe PropertyAdded(propNew1)
      }
    }
  }

}