package io.prods.engine.core.test

import akka.actor.ActorRef
import akka.testkit.{TestActorRef, TestProbe}
import io.prods.engine.core.management.PersistentWarehouseStorage
import io.prods.engine.core.management.PersistentWarehouseStorage.commands._
import io.prods.engine.core.management.PersistentWarehouseStorage.replies._
import io.prods.engine.core.storage.SimpleStorage
import io.prods.engine.core.{EntityId, generateID}

import scala.concurrent.duration._


class PersistentWarehouseStorageSpec extends ActorAsyncSpec {

  "Storage" when {
    def getTestRef = TestActorRef[PersistentWarehouseStorage](PersistentWarehouseStorage.testProps(SimpleStorage(Map
      .empty[EntityId, Option[ActorRef]]), generateID.toString))
    def getSystemRef = system.actorOf(PersistentWarehouseStorage.props)
    "checking entity existence" must {
      "report present entities" in {
        val ref = getSystemRef

        val id = generateID
        val probe = TestProbe()
        within(100.millis) {
          ref ! StoreEntity(id, probe.ref)
          expectMsg(Done)
          ref ! CheckEntityExistence(id)

          expectMsg(Present(id, probe.ref))
        }
      }
      "report present entities with no actor" in {
        val id = generateID
        val ref = system.actorOf(PersistentWarehouseStorage.testProps(
          SimpleStorage[Map[EntityId, Option[ActorRef]]](Map(id -> None)), generateID.toString))

        within(100.millis) {
          ref ! CheckEntityExistence(id)

          expectMsg(PresentNoActor(id))
        }
      }
      "report absent entities" in {
        val ref = getSystemRef

        val id = generateID
        within(100.millis) {
          ref ! CheckEntityExistence(id)

          expectMsg(NotPresent(id))
        }
      }
    }
    "storing entity" must {
      "store non existent entities" in {
        val ref = getTestRef
        val id = generateID
        val probe = TestProbe()

        within(500.millis) {
          ref ! StoreEntity(id, probe.ref)
          expectMsg(Done)
        }
        ref.underlyingActor.storage.items shouldBe Map(id -> Some(probe.ref))
      }
      "do not store existent entities" in {
        val ref = getTestRef
        val id = generateID
        val probe = TestProbe()
        val probeDup = TestProbe()

        within(100.millis) {
          ref ! StoreEntity(id, probe.ref)
          expectMsg(Done)
        }
        within(100.millis) {
          ref ! StoreEntity(id, probeDup.ref)
          expectMsg(StorageError)
        }

        ref.underlyingActor.storage.items shouldBe Map(id -> Some(probe.ref))
        ref.underlyingActor.storage.items should not be Map(id -> Some(probeDup.ref))
      }
      "reply with Done if entity was added" in {
        val id = generateID
        val probe = TestProbe()

        within(100.millis) {
          getSystemRef ! StoreEntity(id, probe.ref)
          expectMsg(Done)
        }
      }
      "reply with Error if entity was not added" in {
        val ref = getSystemRef
        val id = generateID
        val probe = TestProbe()
        val probeDup = TestProbe()

        within(100.millis) {
          ref ! StoreEntity(id, probe.ref)
          expectMsg(Done)
          ref ! StoreEntity(id, probeDup.ref)
          expectMsg(StorageError)
        }
      }
    }
    "disabling entity" must {}
  }
}
