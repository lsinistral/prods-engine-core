package io.prods.engine.core.test

import akka.testkit.{TestActorRef, TestProbe}
import io.prods.engine.core.management.Collector
import io.prods.engine.core.management.Collector.commands.{AbortCollecting, Collect}
import io.prods.engine.core.management.Collector.replies._


class CollectorSpec extends ActorAsyncSpec {

  "collector" when {
    val itemsToCollect = 3
    "collecting" must {
      val probe = TestProbe()
      "store collected items" in {
        val ref = TestActorRef[Collector[Int]](Collector.props[Int](3, probe.ref))

        ref ! Collect(1)
        ref.underlyingActor.items shouldBe Seq(1)
        ref ! Collect(2)
        ref.underlyingActor.items shouldBe Seq(2, 1)
      }
      "count collected items" in {
        val ref = TestActorRef[Collector[Int]](Collector.props[Int](3, probe.ref))

        ref ! Collect(1)
        ref.underlyingActor.itemsToCollect shouldBe 2
        ref ! Collect(2)
        ref.underlyingActor.itemsToCollect shouldBe 1
      }
      "collect raw items of right type" in {
        val ref = TestActorRef[Collector[Int]](Collector.props[Int](3, probe.ref))
        ref ! 1
        ref.underlyingActor.items shouldBe Seq(1)
        ref ! 2
        ref.underlyingActor.items shouldBe Seq(2, 1)
      }
      "collect only right type of items" in {
        val ref = TestActorRef[Collector[Int]](Collector.props[Int](3, probe.ref))
        ref ! Collect("string instead of integer")
        expectMsg(WrongType)
        ref ! "String"
        expectMsg(WrongType)
      }
    }
    "all collected" must {
      "return all items" in {
        val probe = TestProbe()
        val ref = system.actorOf(Collector.props[Int](itemsToCollect, probe.ref))

        (1 to itemsToCollect).foreach { i =>
          ref ! Collect(i)
        }

        probe.expectMsg(Collected(Seq(3, 2, 1)))
      }
      "return items and id" in {
        val probe = TestProbe()
        val ref = system.actorOf(Collector.props[Int](itemsToCollect, probe.ref, 255))

        (1 to itemsToCollect).foreach { i =>
          ref ! Collect(i)
        }

        probe.expectMsg(Collected(Seq(3, 2, 1), 255))
      }
      "not accept more items to collect" in {
        val probe = TestProbe()
        val ref = system.actorOf(Collector.props[Int](itemsToCollect, probe.ref))

        //items to collect
        (1 to itemsToCollect).foreach { i =>
          ref ! Collect(i)
        }
        //extra item
        ref ! Collect(0)
        expectMsg(AllCollected)
      }
      "not accept more raw items to collect" in {
        val probe = TestProbe()
        val ref = system.actorOf(Collector.props[Int](itemsToCollect, probe.ref))

        //items to collect
        (1 to itemsToCollect).foreach { i =>
          ref ! i
        }
        //extra item
        ref ! 0
        expectMsg(AllCollected)
      }
    }
    "aborting" must {
      "return already collected items" in {
        val probe = TestProbe()
        val ref = system.actorOf(Collector.props[Int](itemsToCollect, probe.ref))

        (1 until itemsToCollect).foreach { i =>
          ref ! Collect(i)
        }
        ref ! AbortCollecting

        probe.expectMsg(PartiallyCollected(Seq(2, 1)))

      }
    }

  }
}
