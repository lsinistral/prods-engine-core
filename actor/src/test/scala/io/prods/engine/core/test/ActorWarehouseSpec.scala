package io.prods.engine.core.test

import akka.actor.ActorRef
import akka.testkit.TestProbe
import io.prods.engine.core._
import io.prods.engine.core.actorCommons.{Ping, UnsupportedOperation}
import io.prods.engine.core.management.ActorWarehouse.commands.{CreateEntities, GetEntities}
import io.prods.engine.core.management.ActorWarehouse.replies._
import io.prods.engine.core.management._
import io.prods.engine.core.management.warehouse.{CreationWorker, GettingWorker}
import io.prods.engine.core.storage.SimpleStorage

import scala.concurrent.duration._


class ActorWarehouseSpec extends ActorAsyncSpec {
  "Actor warehouse" when {
    "creating entities" must {
      "return created entities" in {
        val ref = system.actorOf(ActorWarehouse.testProps(system))


        within(100.millis) {
          ref ! CreateEntities(2)

          val respond = expectMsgType[EntitiesCreated]

          respond.entities.keys.head shouldBe a[EntityId]
          respond.entities.values.head shouldBe a[ActorRef]
          respond.entities.size shouldBe 2
        }

      }

    }
    "getting items" must {
      "return found entities" in {
        val ref = system.actorOf(ActorWarehouse.testProps(system))

        within(100.millis) {
          ref ! CreateEntities(2)
          val respond = expectMsgType[EntitiesCreated]

          ref ! GetEntities(respond.entities.keySet)
          val respond2 = expectMsgType[EntitiesFound]

          respond2.entities.keys.head shouldBe a[EntityId]
          respond2.entities.values.head shouldBe a[ActorRef]
          respond2.entities.size shouldBe 2
        }
      }
    }
  }
  "Creation Worker" when {
    def storage = system.actorOf(PersistentWarehouseStorage.props)
    def manager = system.actorOf(ActorManager.props)

    def getWorker(timeout: FiniteDuration = 1.second) = system.actorOf(CreationWorker.props(storage, manager, timeout))
    "initialized" must {
      "respond to multi-entity CreateEntities command" in {
        within(120.millis) {
          (1 to 10).foreach(i => {
            val ref = getWorker()
            ref ! CreateEntities(10)
            val respond = expectMsgType[EntitiesCreated]
            respond.entities.size shouldBe 10
          })
        }
        /*        (1 to 10).foreach(i => {
                  val ref = getWorker
                  ref ! CreateEntities(10)
                  receiveOne(100.millis)
                })*/
        //32.250sec 10x100000

        //        ref ! CreateEntities(1000)
        //        val response = receiveOne(60.seconds)
      }
      "respond to single-entity CreateEntities command" in {
        within(100.millis) {
          val ref = getWorker()
          ref ! CreateEntities(1)
          val respond = expectMsgType[EntitiesCreated]
          respond.entities.size shouldBe 1
        }
      }
      "timeout" in {

        within(120.millis) {
          val ref = getWorker(1.millisecond)
          ref ! CreateEntities(10000)

          expectMsgType[EntitiesNotCreated.type]
        }

      }
      "respond with UnsupportedOperation to invalid commands" in {
        within(100.millis) {
          val ref = getWorker()
          ref ! Ping
          expectMsgType[UnsupportedOperation.type]
        }
      }
    }
  }
  "Getting Worker" must {
    val id = generateID
    val reff = TestProbe().ref
    def getWorker(st: Map[EntityId, Option[ActorRef]] = Map.empty, timeout: FiniteDuration = 1.second): ActorRef = {
      val manager = system.actorOf(ActorManager.props)
      val storage = system.actorOf(PersistentWarehouseStorage.testProps(
        SimpleStorage(st), generateID.toString))
      system.actorOf(GettingWorker.props(storage, manager))
    }
    "return entity with ref having only id in state" in {
      val ref = getWorker(Map(id -> None))


      ref ! GetEntities(Set(id))

      val got = expectMsgType[EntitiesFound].entities

      got.keys.head shouldEqual id
      got.size shouldBe 1
    }
    "return same entity having it in state" in {
      val ref = getWorker(Map(id -> Some(reff)))

      ref ! GetEntities(Set(id))

      val got = expectMsgType[EntitiesFound].entities

      got.head shouldBe (id -> reff)
      got.size shouldBe 1
    }
    "respond with UnsupportedOperation to invalid commands" in {
      within(100.millis) {
        val ref = getWorker()
        ref ! Ping
        expectMsgType[UnsupportedOperation.type]
      }
    }

  }

}
