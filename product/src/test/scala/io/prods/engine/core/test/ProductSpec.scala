package io.prods.engine.core.test

import io.prods.engine.core.EntityId
import io.prods.engine.core.entity._
import io.prods.engine.core.product._
import io.prods.engine.core.test.share.{MockTypeclassProperties, UnitSpec}
import org.scalatest.{Matchers, WordSpecLike}


trait ProductBehavior extends WordSpecLike with Matchers with MockTypeclassProperties {

  def relational[E <: Entity[EntityId] with Relations[EntityId]](getEmptyProduct: => E)
                                                                (implicit evidence: RelationalEntity[E]): Unit = {
    "relational" must {
      import RelationalEntity.EnableVariants._
      "create variants" in {
        val ent = getEmptyProduct

        val (result, variant) = ent.createVariant()

        variant.isVariant shouldBe true
        result.relations.variants shouldBe a[Some[_]]
        variant.relations.parent shouldBe a[Some[_]]

        result.relations.variants.get.size shouldBe 1
        result.relations.variants.get.head shouldBe a[EntityId]
        variant.relations.parent.get shouldBe a[EntityId]

        result.relations.variants.get.head shouldEqual variant.id
        variant.relations.parent.get shouldEqual result.id
      }
      "break relations" in {
        val (parent, variant) = getEmptyProduct.createVariant()
        val (result1, result2) = parent.breakRelations(variant)

        result1.relations.variants shouldBe None
        result2.relations.parent shouldBe None
        result2.isVariant shouldBe false

        val (parent_edit, variant2) = parent.createVariant()
        val (result3, result4) = parent_edit.breakRelations(variant2)

        result3.relations.variants shouldNot contain(result4.id)
        result4.relations.parent shouldBe None
        result4.isVariant shouldBe false

      }
    }
  }

}

class ProductSpec extends UnitSpec with MockTypeclassProperties with ProductBehavior {


  import Entity.Ops._


  "Product" when {
    import Product._
    val getEmptyProduct = Product.empty[Int]
    val getFilledProduct = Product.empty[Int].and(propInState1, propInState2, propInState3)

    val initialState = Map(
      propInState1.id -> propInState1,
      propInState2.id -> propInState2,
      propInState3.id -> propInState3)

    val initialMemory = Seq(PropertyAdded(propInState3), PropertyAdded(propInState2), PropertyAdded
    (propInState1))
    val initialIds = Set(propInState1.id, propInState2.id, propInState3.id)
    "extensible" must {
      "add new properties" in {
        val ent = Product.empty[Int]

        val result = ent.and(propNew1)

        result.history.memory shouldBe Seq(PropertyAdded(propNew1))

      }
    }
    "changeable" must {
      "change existent properties" in {
        val ent = getFilledProduct

        val result = ent.change(propInStateChanges1)

        result.history.memory shouldBe PropertyChanged(propInStateChanges1) +: initialMemory
        result.history.ids shouldBe initialIds

        result.view.state shouldBe (initialState + (propInState1.id -> propInStateChanges1))
      }
    }
    behave like relational(getEmptyProduct)
  }

  "Stateless Product" when {
    import StatelessProduct._
    behave like relational(StatelessProduct.empty[Int])
  }
}
