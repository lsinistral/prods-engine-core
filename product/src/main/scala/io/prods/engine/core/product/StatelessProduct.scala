package io.prods.engine.core.product

import io.prods.engine.core.entity._
import io.prods.engine.core.entity.component.HistoryComponent._
import io.prods.engine.core.entity.component._
import io.prods.engine.core.entity.property._
import io.prods.engine.core.{EntityId, generateID}


case class StatelessProduct[K](val id: EntityId = generateID,
                               val history: EntityHistory[K] = EntityHistory.empty[K](),
                               val relations: EntityRelations[EntityId] = EntityRelations.empty[EntityId])
  extends Entity[EntityId] with Emptiness with History[K] with Relations[EntityId] {
  override def toString =
    s"Product with id $id \n" +
      s"\t $history \n" +
      s"\t Relations: \n $relations"

  def isEmpty: Boolean = history.isEmpty && relations.isEmpty
}


object StatelessProduct {


  def empty[K] = StatelessProduct[K]()

  trait Extensible[K] extends ExtensibleEntity[KeyProp[K], StatelessProduct[K]] {


    object ExtensibleHistory extends EntityHistory.ExtensibleEntityHistory[K]

    def addProperties(props: Seq[KeyProp[K]])(entity: StatelessProduct[K]): StatelessProduct[K] =
      entity.copy(history = ExtensibleHistory.addProperties(props)(entity.history))


    def addProperty(prop: KeyProp[K])(entity: StatelessProduct[K]): StatelessProduct[K] =
      entity.copy(history = ExtensibleHistory.addProperty(prop)(entity.history))
  }

  trait Changeable[K] extends ChangeableEntity[KeyProp[K], StatelessProduct[K]] {


    object ChangeableHistory extends EntityHistory.ChangeableEntityHistory[K]

    def changeProperties(props: Seq[KeyProp[K]])(entity: StatelessProduct[K]): StatelessProduct[K] =
      entity.copy(history = ChangeableHistory.changeProperties(props)(entity.history))

    def changeProperty(prop: KeyProp[K])(entity: StatelessProduct[K]): StatelessProduct[K] =
      entity.copy(history = ChangeableHistory.changeProperty(prop)(entity.history))
  }

  trait Relational[K] extends RelationalEntity[StatelessProduct[K]] {
    def createVariant(entity: StatelessProduct[K]): (StatelessProduct[K], StatelessProduct[K]) = {
      val id = generateID
      val base: StatelessProduct[K] = entity.copy(relations = entity.relations and id)
      val variant = StatelessProduct[K](id = id, relations = EntityRelations(parent = Some(entity.id)))
      (base, variant)
    }

    def breakRelations(parent: StatelessProduct[K], variant: StatelessProduct[K]):
    (StatelessProduct[K], StatelessProduct[K]) = {
      val isRelated = (parent.relations isParentOf variant.id) && (variant.relations isChildOf parent.id)
      if (isRelated)
        (parent.copy(relations = parent.relations but variant.id),
          variant.copy(relations = variant.relations breakWith parent.id))
      else (parent, variant)
    }

    def isVariant(entity: StatelessProduct[K]): Boolean =
      entity.relations.parent.isDefined
  }


  trait RegularProduct[I]
    extends Extensible[I]
            with ExtensibleHistoryValidation[I, StatelessProduct[I]]
            with Changeable[I]
            with ChangeableHistoryValidation[I, StatelessProduct[I]]

  implicit object Int extends RegularProduct[Int] with Relational[Int]

}
