package io.prods.engine.core.product

import io.prods.engine.core.entity.Entity.EntityOps
import io.prods.engine.core.entity._
import io.prods.engine.core.entity.component.HistoryComponent._
import io.prods.engine.core.entity.component._
import io.prods.engine.core.entity.property.KeyProp
import io.prods.engine.core.{EntityId, generateID}


/**
  * Product representation in system
  *
  * @param id product identificator must be unique within system
  */
case class Product[K](id: EntityId = generateID,
                      history: EntityHistory[K] = EntityHistory.empty[K](),
                      view: EntityView[K] = EntityView.empty[K](),
                      relations: EntityRelations[EntityId] = EntityRelations.empty[EntityId])
  extends Entity[EntityId] with History[K] with Relations[EntityId] {
  override def toString =
    s"Product with id $id \n" +
      s"\t $history \n" +
      s"\t $view\n" +
      s"\t Relations: \n $relations"
}


object Product {


  def empty[K] = Product[K]()


  trait Extensible[K] extends ExtensibleEntity[KeyProp[K], Product[K]] {

    object ExtensibleHistory extends EntityHistory.ExtensibleEntityHistory[K]

    object ExtensibleView extends EntityView.ExtensibleEntityView[K]

    def addProperties(props: Seq[KeyProp[K]])(entity: Product[K]): Product[K] =
      entity.copy(
        view = ExtensibleView.addProperties(props)(entity.view),
        history = ExtensibleHistory.addProperties(props)(entity.history))


    def addProperty(prop: KeyProp[K])(entity: Product[K]): Product[K] =
      entity.copy(
        view = ExtensibleView.addProperty(prop)(entity.view),
        history = ExtensibleHistory.addProperty(prop)(entity.history))

  }

  trait Changeable[K] extends ChangeableEntity[KeyProp[K], Product[K]] {

    object ChangeableHistory extends EntityHistory.ChangeableEntityHistory[K]

    object ChangeableView extends EntityView.ChangeableEntityView[K]

    def changeProperties(props: Seq[KeyProp[K]])(entity: Product[K]): Product[K] =
      entity.copy(
        view = ChangeableView.changeProperties(props)(entity.view),
        history = ChangeableHistory.changeProperties(props)(entity.history))

    def changeProperty(prop: KeyProp[K])(entity: Product[K]): Product[K] =
      entity.copy(
        view = ChangeableView.changeProperty(prop)(entity.view),
        history = ChangeableHistory.changeProperty(prop)(entity.history))
  }

  trait RelationalProduct[K] extends RelationalEntity[Product[K]] {
    def createVariant(entity: Product[K]): (Product[K], Product[K]) = {
      val id = generateID
      val base: Product[K] = entity.copy(relations = entity.relations and id)
      val variant = Product[K](id = id, relations = EntityRelations(parent = Some(entity.id)))
      (base, variant)
    }

    def breakRelations(parent: Product[K], variant: Product[K]): (Product[K], Product[K]) = {
      val isRelated = (parent.relations isParentOf variant.id) && (variant.relations isChildOf parent.id)
      if (isRelated)
        (parent.copy(relations = parent.relations but variant.id),
          variant.copy(relations = variant.relations breakWith parent.id))
      else (parent, variant)
    }

    def isVariant(entity: Product[K]): Boolean =
      entity.relations.parent.isDefined
  }


  trait RegularProduct[I]
    extends Extensible[I]
            with ExtensibleHistoryValidation[I, Product[I]]
            with Changeable[I]
            with ChangeableHistoryValidation[I, Product[I]]

  implicit object Int extends RegularProduct[Int] with RelationalProduct[Int]

}