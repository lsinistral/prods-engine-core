package io.prods.engine.core.product

import io.prods.engine.core.entity.Emptiness
import io.prods.engine.core.product.RelationalEntity.EntityRelationsEvent
import io.prods.engine.core.product.RelationalEntity.events._


case class EntityRelations[EID](parent: Option[EID] = None,
                                variants: Option[Set[EID]] = None,
                                events: Seq[EntityRelationsEvent] = Seq.empty) extends Emptiness {
  def isEmpty = events.isEmpty && parent.isEmpty && variants.isEmpty
}

object EntityRelations {
  def empty[K] = EntityRelations[K]()


  implicit class Relations[EID](self: EntityRelations[EID]) {
    def and(id: EID): EntityRelations[EID] =
      self.copy(variants = Some(self.variants.getOrElse(Set.empty) + id))

    def <--(id: EID): EntityRelations[EID] = and(id)


    def but(id: EID): EntityRelations[EID] =
      self.copy(variants = self.variants.map(_ - id).filter(_.nonEmpty), events = VariantRemoved(id) +: self.events)

    def --|(id: EID): EntityRelations[EID] = but(id)


    def breakWith(parentId: EID): EntityRelations[EID] =
      self.copy(parent = None, events = RelationsBroken(parentId) +: self.events)


    def isChildOf(parentId: EID): Boolean =
      self.parent.contains(parentId)

    def isParentOf(id: EID): Boolean =
      self.variants.exists(_.contains(id))

  }

}

trait Relations[EID] {
  val relations: EntityRelations[EID]
}
