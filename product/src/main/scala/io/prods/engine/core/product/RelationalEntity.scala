package io.prods.engine.core.product

import io.prods.engine.core.entity.{Entity, EntityEvent}


trait RelationalEntity[E <: Entity[_]] {
  def createVariant(entity: E): (E, E)

  def breakRelations(parent: E, variant: E): (E, E)

  def isVariant(entity: E): Boolean
}


object RelationalEntity {

  trait RelationalEntityOps {

    implicit class Ops[E <: Entity[_]](entity: E)
                                      (implicit evidence: RelationalEntity[E]) {

      def createVariant(): (E, E) = evidence.createVariant(entity)

      def breakRelations(variant: E): (E, E) = evidence.breakRelations(entity, variant)

      def isVariant = evidence.isVariant(entity)
    }

  }

  object EnableVariants extends RelationalEntityOps

  trait EntityRelationsEvent extends EntityEvent

  object events {

    final case class VariantCreated[ID](variantId: ID) extends EntityRelationsEvent

    final case class VariantRemoved[ID](variantId: ID) extends EntityRelationsEvent

    final case class RelationsBroken[ID](baseId: ID) extends EntityRelationsEvent

  }

}